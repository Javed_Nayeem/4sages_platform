<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Input;


class CourseController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index($id) {

        if (!is_numeric($id)) {
            $temp = "";
            if ($id == "lifescience") $temp = "Life Science";
            else if ($id == "earthscience") $temp = "Earth Science";
            if ($id == "physicalscience") $temp = "Physical Science";

            $course_data = DB::table('courses')
                ->select('course_id')
                ->where('course_name', '=', $temp)
                ->first();

            $id = $course_data->course_id;
        }


        $users = DB::table('users')
            ->select('users.id', 'users.name', 'users.first_name', 'users.last_name', 'users.email' )
            ->where('users.id', '!=', Auth::id())
            ->get();


        $course_data = DB::table('courses')
            ->where('course_id', '=', $id)
            ->first();

        $syllabus_data = DB::table('syllabus')
            ->where('course_id', '=', $id)
            ->get();

        $student_data = DB::table('course_student')
            ->where('course_id', '=', $id)
            ->leftJoin('users', 'users.id', '=', 'course_student.student_id')
            ->get();


        $forum_data = DB::table('forum')
            ->where('course_id', '=', $id)
            ->leftJoin('users', 'users.id', '=', 'forum.user_id')
            ->get();

        $invitation_data = DB::table('course_invitation')
            ->where('course_id', '=', $id)
            ->get();

        return view('4sages.course.index', [
            'course_data' => $course_data,
            'syllabus_data' => $syllabus_data,
            'forum_data' => $forum_data,
            'student_data' => $student_data,
            'users' => $users,
            'id' => $id,
            'invitation_data' => $invitation_data,
        ]);

    }


    public function add(Request $request) {

        $userId = Auth::user()->id;
        create_directory();
        $course_id = DB::table('courses')->insertGetId(
            array(
                'course_name' => $request['course_name'],
                'description' => $request['description'],
                'overview' => $request['overview'],
                'tutor_id' => $userId,
                'course_status' => 1
            )
        );

        //$file = array('image' => Input::file('image'));
        $destinationPath = 'course';

        if (Input::file('image')) {
            $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = 'course_'. $course_id .'.'.$extension;
            Input::file('image')->move($destinationPath, $fileName);

            $this->createThumbnail($fileName);

            DB::table('courses')
                ->where('course_id', $course_id)
                ->update([
                    'image_title' => $fileName,
                ]);
        }



        return redirect("/course/" . $course_id);

    }


    function createThumbnail($postTitle) {

        // configure with favored image driver (gd by default)
        Image::configure(array('driver' => 'gd'));

        // creating thumbnail image
        $original_image = "course/" . $postTitle;

        $thumbnail_image = "course/thumbnail/" . $postTitle;
        $preview_image = "course/preview/" . $postTitle;


        $image = Image::make($original_image)->resize(270, 273)->save($thumbnail_image);
        $image = Image::make($original_image)->resize(400, 362)->save($preview_image);


    }


    public function edit(Request $request) {

        $data = $request->input('params');
        DB::table('courses')
            ->where('course_id', $data ["course_id"])
            ->update([
                'course_name' => $data['course_name'],  //database column name = form field id
                'description'=> $data['description'],
                'overview'=> $data['course_overview']
            ]);
        echo json_encode("Success");
    }


    public function  editsyllabus(Request $request) {
        $data2= $request->input('params');
        DB::table('syllabus')
            ->where('week_no',"=", $data2["week_no"])
            ->update([
                'unit_title' => $data2['edit_unit_title'],  //database column name = form field id
                'unit_overview'=> $data2['edit_unit_overview'],

            ]);
        echo json_encode("Success");
    }


    public function all_course() {

        $courses = DB::table('courses')
            ->paginate(12);

        return view('4sages.course.courselist', [
            'courses' => $courses,
        ]);

    }


    public function syllabus($id) {
        return view('4sages.course.testsyllabus', [
            'course_id' => $id
        ]);

    }


    public function delete (Request $request) {

        $data = $request->input('params');
        $id = $data['course_id'];

        $course_files = DB::table('courses')
            ->select('image_title')
            ->where('course_id', '=', $id)
            ->first();

        if ($course_files->image_title != "curriculum_default.jpg") {

            $delete_path = "course/" . $course_files->image_title;
            File::delete($delete_path);

            $delete_path = "course/preview/" . $course_files->image_title;
            File::delete($delete_path);

            $delete_path = "course/thumbnail/" . $course_files->image_title;
            File::delete($delete_path);
        }


        DB::table('courses')->where('course_id', '=', $id)->delete();
        DB::table('course_student')->where('course_id', '=', $id)->delete();

        echo json_encode("success");

    }


    public function send_message(Request $request) {

        $data = $request->input('params');
        $userId = Auth::user()->id;

        $course_id = DB::table('forum')->insertGetId(
            array(
                'user_id' => $userId,
                'course_id' => $data['course_id'],
                'message' => $data['message']
            )
        );
        echo json_encode("success");
    }


    public function add_student(Request $request) {

        $data = $request->input('params');
        $course_id = $data['course_id'];
        $students = "";
        if (isset($data['student_list'])) {
            $students = $data['student_list'];
        }


        DB::table('course_student')->where('course_id', '=', $course_id)->delete();
        if (isset($data['student_list'])) {
            foreach ($students as $student) {

                DB::table('course_student')->insert([
                        'course_id' => $course_id,
                        'student_id' => $student
                    ]
                );

            }
        }

        echo json_encode(count($students));

    }


    public function  add_syllabus(Request $request) {
        create_directory();
        $userId = Auth::user()->id;
        //$data= $request->input('params');

        $syllabus_id = DB::table('syllabus')->insertGetId(
            array(
                'unit_title' => $request['unit_title'],
                'unit_overview' => $request['unit_overview'],
                'course_id' => $request['course_id'],
                'tutor_id' => $userId
            )
        );

        $file = Input::file('unit_image');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            $post_mime_type = $file->getMimeType();
            if (strpos($post_mime_type, 'image') === 0) {
                $destinationPath = '/public/syllabus/';
                $extension = $file->getClientOriginalExtension();
                $fileName = 'syllabus_'. $syllabus_id .'.'.$extension;
                $request->file('unit_image')->move(base_path() . $destinationPath, $fileName);
                $this->createSyllabusThumbnail($fileName);
                DB::table('syllabus')
                    ->where('id',"=", $syllabus_id)
                    ->update([
                        'unit_image' => $fileName
                    ]);
            }
        }
        return redirect("/course/tutorial/" . $syllabus_id );
    }


    function createSyllabusThumbnail($postTitle) {
        // configure with favored image driver (gd by default)
        Image::configure(array('driver' => 'gd'));

        // creating thumbnail image
        $original_image = "syllabus/" . $postTitle;
        $preview_image = "syllabus/preview/" . $postTitle;
        $image = Image::make($original_image)->resize(371, 216)->save($preview_image);
    }


    public function delete_syllabus (Request $request) {

        $data = $request->input('params');
        $id = $data['syllabus_id'];

        $course_id = DB::table('syllabus')
            ->where('id', '=', $id)
            ->first();

        DB::table('syllabus')->where('id', '=', $id)->delete();


        echo json_encode($course_id->course_id);

    }


    public function delete_quiz ($id) {

        $question_id = DB::table('quiz_question')
            ->where('quiz_id', '=', $id)
            ->get();
        foreach($question_id as $question){
            DB::table('quiz_option')->where('question_id', '=', $question->id)->delete();
            DB::table('quiz_question')->where('id', '=', $question->id)->delete();
        }
        DB::table('quiz')->where('id', '=', $id)->delete();

        echo json_encode("success");

    }


    public function tutorial($id) {

        $syllabus_data = DB::table('syllabus')
            ->where('id', '=', $id)
            ->first();

        $content_data = DB::table('syllabus_content')
            ->where('syllabus_id', '=', $id)
            ->get();

        $flashcard_data = DB::table('content_flashcard')
            ->where('syllabus_id', '=', $id)
            ->get();

        $files_data = DB::table('content_files')
            ->where('syllabus_id', '=', $id)
            ->get();

        $quiz_data = DB::table('quiz_view')
            ->where('syllabus_id', '=', $id)
            ->get();


        return view('4sages.course.tutorial', [
            'syllabus_data' => $syllabus_data,
            'content_data' => $content_data,
            'flashcard_data' => $flashcard_data,
            'files_data' => $files_data,
            'quiz_data' => $quiz_data,
        ]);
    }


    public function add_syllabus_content(Request $request) {

        $userId = Auth::user()->id;
        create_directory();
        $date = date('Y-m-d H:i:s');
        $fileName = "default";
        $width = 0;
        $height = 0;
        $media_id = 0;


        $file = $request->file('content_media');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);

        if ($validator->passes()) {

            $post_mime_type = $file->getMimeType();

            if (strpos($post_mime_type, 'image') === 0) {
                $data = getimagesize($file);
                $width = $data[0];
                $height = $data[1];

                $destinationPath = '/public/content/';
                $fileName = substr(rand(), 0, 5) . '_' . preg_replace('/\s+/', '_', $file->getClientOriginalName());
                $request->file('content_media')->move($destinationPath, $fileName);
            }

            else if (strpos($post_mime_type, 'video') === 0) {
                $destinationPath = '/public/content/';
                $fileName = substr(rand(), 0, 5) . '_' . preg_replace('/\s+/', '_', $file->getClientOriginalName());
                $request->file('content_media')->move(base_path() . $destinationPath, $fileName);
            }

            $media_id = DB::table('media')->insertGetId([
                    'author_id' => $userId,
                    'post_title' => $fileName,
                    'post_mime_type' => $post_mime_type,
                    'width' => $width,
                    'height' => $height,
                    'created_at' => $date,
                    'updated_at' => $date
                ]
            );

        }

        //end of media upload

        if (isset($request['youtube_link']) && !empty($request['youtube_link'])) {
            $fileName = $request['youtube_link'];
        }

        $content_id = DB::table('syllabus_content')->insertGetId([
                'content_title' => $request['content_title'],
                'content_description' => $request['content_area'],
                'media' => $fileName,
                'author_id' => $userId,
                'syllabus_id' => $request['syllabus_id'],
                'created_at' => $date,
                'updated_at' => $date
            ]
        );




        if (isset($request['start_1']) && !empty($request['start_1']) &&
            isset($request['question_1']) && !empty($request['question_1']) &&
            isset($request['q1_option_1']) && !empty($request['q1_option_1']) &&
            isset($request['q1_option_2']) && !empty($request['q1_option_2']) &&
            isset($request['q1_option_3']) && !empty($request['q1_option_3'])) {

            $start_time_1 = $request['start_1'];
            $question_1 = $request['question_1'];
            $q1_option_1 = $request['q1_option_1'];
            $q1_option_2 = $request['q1_option_2'];
            $q1_option_3 = $request['q1_option_3'];
            $ans_1 = $request['option_1_select_ans'];

            DB::table('syllabus_message')->insert([
                    'syllabus_id' => $request['syllabus_id'],
                    'content_id' => $content_id,
                    'start' => $start_time_1,
                    'end' => 0,
                    'question' => $question_1,
                    'option_1' => $q1_option_1,
                    'option_2' => $q1_option_2,
                    'option_3' => $q1_option_3,
                    'answer' => $ans_1
                ]
            );
        }


        if (isset($request['start_2']) && !empty($request['start_2']) &&
            isset($request['question_2']) && !empty($request['question_2']) &&
            isset($request['q2_option_1']) && !empty($request['q2_option_1']) &&
            isset($request['q2_option_2']) && !empty($request['q2_option_2']) &&
            isset($request['q2_option_3']) && !empty($request['q2_option_3']) ) {
            $start_time_2 = $request['start_2'];
            $question_2 = $request['question_2'];
            $q2_option_1 = $request['q2_option_1'];
            $q2_option_2 = $request['q2_option_2'];
            $q2_option_3 = $request['q2_option_3'];
            $ans_2 = $request['option_2_select_ans'];

            DB::table('syllabus_message')->insert([
                    'syllabus_id' => $request['syllabus_id'],
                    'content_id' => $content_id,
                    'start' => $start_time_2,
                    'end' => 0,
                    'question' => $question_2,
                    'option_1' => $q2_option_1,
                    'option_2' => $q2_option_2,
                    'option_3' => $q2_option_3,
                    'answer' => $ans_2
                ]
            );
        }


        if (isset($request['start_3']) && !empty($request['start_3']) &&
            isset($request['question_3']) && !empty($request['question_3']) &&
            isset($request['q3_option_1']) && !empty($request['q3_option_1']) &&
            isset($request['q3_option_2']) && !empty($request['q3_option_2']) &&
            isset($request['q3_option_3']) && !empty($request['q3_option_3'])) {

            $start_time_3 = $request['start_3'];
            $question_3 = $request['question_3'];
            $q3_option_1 = $request['q3_option_1'];
            $q3_option_2 = $request['q3_option_2'];
            $q3_option_3 = $request['q3_option_3'];
            $ans_3 = $request['option_3_select_ans'];

            DB::table('syllabus_message')->insert([
                    'syllabus_id' => $request['syllabus_id'],
                    'content_id' => $content_id,
                    'start' => $start_time_3,
                    'end' => 0,
                    'question' => $question_3,
                    'option_1' => $q3_option_1,
                    'option_2' => $q3_option_2,
                    'option_3' => $q3_option_3,
                    'answer' => $ans_3
                ]
            );
        }
        /*
                echo "start time : " . $start_time_1 . "<br>";
                echo "question : " . $question_1 . "<br>";
                echo "option 1 : " . $q1_option_1 . "<br>";
                echo "option 2 : " . $q1_option_2 . "<br>";
                echo "option 3 : " . $q1_option_3 . "<br>";
                echo "ans 1 : " . $ans_1 . "<br>";
        */


        return redirect("/course/tutorial/" . $request['syllabus_id'] );

    }


    public function getMessage($id) {

        $message_data = DB::table('syllabus_message')
            ->where('syllabus_id', '=', $id)
            ->get();

        echo json_encode($message_data);

    }


    public function add_flashcard(Request $request) {
        $userId = Auth::user()->id;
        create_directory();

        //echo $request['flashcard_syllabus_id'];

        $flashcard_id = DB::table('content_flashcard')->insertGetId(
            array(
                'syllabus_id' => $request['flashcard_syllabus_id'],
                'flashcard_title' => $request['flashcard_title'],
                'flashcard_description' => $request['flashcard_description'],
                'media' => "media_link",
                'user_id' => $userId
            )
        );




        $file = Input::file('image');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {

            $post_mime_type = $file->getMimeType();

            if (strpos($post_mime_type, 'image') === 0) {
                $destinationPath = '/public/flashcard/';
                $extension = $file->getClientOriginalExtension();
                $fileName = 'flashcard_'. $flashcard_id .'.'.$extension;
                $request->file('image')->move(base_path() . $destinationPath, $fileName);

                DB::table('content_flashcard')
                    ->where('id',"=", $flashcard_id)
                    ->update([
                        'media' => $fileName

                    ]);
            }
        }

        return redirect("/course/tutorial/" . $request['flashcard_syllabus_id'] );
    }


    public function addContentFile(Request $request) {
        $userId = Auth::user()->id;
        create_directory();

        $file = Input::file('image');
        $rules = array('file' => 'required');
        $validator = Validator::make(array('file' => $file), $rules);

        $destinationPath = '/public/content/files/';
        $extension = $file->getClientOriginalExtension();
        $fileName = time() . '.' .$extension;
        $request->file('image')->move(base_path() . $destinationPath, $fileName);

        $post_mime_type = mime_content_type(base_path() . '/public/content/files/' . $fileName);
        $title = "";
        if (isset($request['file_title']) && !empty($request['file_title'])) {
            $title = $request['file_title'];
        }

        DB::table('content_files')->insert([
                'syllabus_id' => $request['syllabus_id'],
                'file_title' => $title,
                'file_name' => $fileName,
                'post_mime_type' => $post_mime_type,
                'user_id' => $userId
            ]
        );

        return redirect("/course/tutorial/" . $request['syllabus_id'] );
    }


    public function tutorialComplete(Request $request) {

        $userId = Auth::user()->id;
        $data = $request->input('params');

        $exist = DB::table('tutorial_completed')->where(['c_time'=>$data['c_time']])->first();
        if(count($exist)>0) {
            if ($exist->completed_percentage != 100) {
                DB::table('tutorial_completed')
                    ->where('c_time', $data['c_time'])
                    ->update([
                        'completed_percentage' => $data['completed_percentage'],
                    ]);
            }
        }
        else  {
            DB::table('tutorial_completed')->insert([
                'student_id' => $userId,
                'syllabus_id' => $data['syllabus_id'],
                'completed_percentage' => $data['completed_percentage'],
                'c_time' => $data['c_time'],
            ]);
        }
        return 'success';
    }

}




