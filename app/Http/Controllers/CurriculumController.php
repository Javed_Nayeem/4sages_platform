<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Input;

class CurriculumController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
    }


    public function index($id) {

        if (!is_numeric($id)) {
            $temp = "";
            if ($id == "lifescience") $temp = "Life Science";
            else if ($id == "earthscience") $temp = "Earth Science";
            if ($id == "physicalscience") $temp = "Physical Science";

            $course_data = DB::table('courses')
                ->select('course_id')
                ->where('course_name', '=', $temp)
                ->first();

            $id = $course_data->course_id;
        }


        $users = DB::table('users')
            ->select('users.id', 'users.name', 'users.first_name', 'users.last_name', 'users.email' )
            ->where('users.id', '!=', Auth::id())
            ->get();


        $course_data = DB::table('courses')
            ->where('course_id', '=', $id)
            ->first();

        $syllabus_data = DB::table('syllabus')
            ->where('course_id', '=', $id)
            ->get();

        $student_data = DB::table('course_student')
            ->where('course_id', '=', $id)
            ->leftJoin('users', 'users.id', '=', 'course_student.student_id')
            ->get();


        $forum_data = DB::table('forum')
            ->where('course_id', '=', $id)
            ->leftJoin('users', 'users.id', '=', 'forum.user_id')
            ->get();

        $invitation_data = DB::table('course_invitation')
            ->where('course_id', '=', $id)
            ->get();

        return view('4sages.course.index', [
            'course_data' => $course_data,
            'syllabus_data' => $syllabus_data,
            'forum_data' => $forum_data,
            'student_data' => $student_data,
            'users' => $users,
            'id' => $id,
            'invitation_data' => $invitation_data,
        ]);

    }


    public function all_course() {

        $courses = DB::table('courses')
            ->paginate(12);

        return view('4sages.course.courselist', [
            'courses' => $courses,
        ]);

    }

}
