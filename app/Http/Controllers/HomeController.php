<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function testupload(Request $request) {
        echo json_encode("image success");
    }


    public function test() {

        $quizes = DB::table('quiz_view')
            ->select('quiz_view_id')
            ->get();

        foreach ($quizes as $quiz) {
            $results = DB::table('quiz_score')
                ->where('quiz_id', $quiz->quiz_view_id)
                ->orderBy('score','DESC')
                ->get();

            $p= count($results);
            $position = 1;

            foreach ($results as $result) {
                $l= $p - $position;
                if ($p != 0) $percentile = ($l/$p)*100;
                else $percentile = 100;
                $position++;

                DB::table('quiz_score')
                    ->where('quiz_score_id', $result->quiz_score_id)
                    ->update([
                        'percentile' => $percentile
                    ]);
                //echo $result->quiz_score_id . ' ======== ' . $result->score. '========' . $percentile ."<br>";
            }
        }
    }
}
