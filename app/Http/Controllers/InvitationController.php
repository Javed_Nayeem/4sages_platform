<?php

namespace App\Http\Controllers;
use App\Mail\SagesMail;
use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Mail;

class InvitationController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {

        $data = $request->input('params');
        $email = $data['email'];
        $userId = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        $invitation_data = array(
            "invitation_course_id" => $data['course_id'],
            "invitation_course_name" => getCourseName($data['course_id']),
            "invitation_from_name" => getName($userId),
            "invitation_from_email" => "4sages.test@4sages.com",
            "invitation_link" => "www.4sages.com",
        );

        DB::table('course_invitation')->insert([
                'tutor_id' => $userId,
                'course_id' => $data['course_id'],
                'invitation_email' => $email,
                'created_at' => $date,
            ]
        );

        Mail::to($email)->send(new SagesMail($invitation_data));

        echo json_encode("Success");
    }


    public function testmail() {

        $invitation_data = array(
            "invitation_course_id" => 3,
            "invitation_course_name" => "Demo Course Name",
            "invitation_from_name" => "Demo Teacher Name",
            "invitation_from_email" => "4sages.test@4sages.com",
            "invitation_link" => "www.4sages.com",
        );


        Mail::to('javed@nulytics.com')->send(new SagesMail($invitation_data));

        echo "mail sent";
    }

}
