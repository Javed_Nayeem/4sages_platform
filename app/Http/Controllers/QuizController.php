<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use League\Flysystem\Exception;

class QuizController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index($id) {

        return view('4sages.quiz.index', [
            'course_id' => $id,
        ]);

    }


    public function add_quiz(Request $request) {
        //echo "hi";





        $userId = Auth::user()->id;
        create_directory();

        $questions = $request['question'];
        $options = $request['option'];
        $answers = $request['answer'];
        $explanations = $request['explanation'];
        $blog_name = $request['blog_name'];
        $blog_link = $request['blog_link'];

        //echo "hi";

        $quiz_view_id = DB::table('quiz_view')->insertGetId(
            array(
                'course_topic_id' => $request['course_topic_id'],
                'syllabus_id' => $request['course_topic_id'],
                'tutor_id' => $userId,
            )
        );


        for ($i=0; $i<count($blog_link); $i++) {

            DB::table('quiz_blog')->insert([
                    'quiz_view_id' => $quiz_view_id,
                    'blog_name' => $blog_name[$i],
                    'blog_link' => $blog_link[$i],
                ]
            );
        }


        for ($i=0, $j=0; $i<count($questions); $i++, $j+=3) {
            /*
                        echo $questions[$i] . ":<br>";
                        echo $options[$j] . " ----- " . $options[$j+1] . " ----- " . $options[$j+2] . "<br>";
                        echo "answer : " . $answers[$i];
                        echo "<br><br><br>";
              */

            if (isset($questions[$i]) && !empty($questions[$i])) {

                $medianame = "media_" . ($i+1);
                $explanation_medianame = "explanation_media_" . ($i+1);
                $fileName = "";
                $explanation_filename = "";

                if(Input::hasFile($medianame)){
                    $file = Input::file($medianame);
                    $destinationPath = '/public/questionimage/';
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() .'.'.$extension;
                    $request->file($medianame)->move(base_path() . $destinationPath, $fileName);
                }

                if(Input::hasFile($explanation_medianame)){
                    $file = Input::file($explanation_medianame);
                    $destinationPath = '/public/explanation/';
                    $extension = $file->getClientOriginalExtension();
                    $explanation_filename = time() .'.'.$extension;
                    $request->file($explanation_medianame)->move(base_path() . $destinationPath, $explanation_filename);
                }


                DB::table('quiz')->insert([
                        'quiz_view_id' => $quiz_view_id,
                        'question' => $questions[$i],
                        'option_1' => $options[$j],
                        'option_2' => $options[$j+1],
                        'option_3' => $options[$j+2],
                        'answer' => $answers[$i],
                        'media' => $fileName,
                        'explanation' => $explanations[$i],
                        'explanation_media' => $explanation_filename,
                    ]
                );


            }

        }

        return redirect("/course/tutorial/" . $request['course_topic_id']);

    }


    public function course_quiz(Request $request) {

        $quiz_view_id = $request['course_quiz'];

        $quiz_data = DB::table('quiz')
            ->where('quiz_view_id', '=', $quiz_view_id)
            ->get();

        $blog_data = DB::table('quiz_blog')
            ->where('quiz_view_id', '=', $quiz_view_id)
            ->get();

        $syllabus_id = DB::table('quiz_view')
            ->select('course_topic_id', 'syllabus_id')
            ->where('quiz_view_id', '=', $quiz_view_id)
            ->first();


        return view('4sages.quiz.quiz', [
            'quiz_data' => $quiz_data,
            'quiz_view_id' => $quiz_view_id,
            'syllabus_id' => $syllabus_id,
            'blog_data' => $blog_data,
        ]);
    }


    public function storeQuizResult (Request $request) {
        $data = $request->input('params');
        $userId = Auth::user()->id;
        $quiz_view_id = $data["quiz_view_id"];
        $score = $data["score"];
        $syllabus_id = $data["syllabus_id"];

        DB::table('quiz_score')->insert([
                'syllabus_id' => $syllabus_id,
                'quiz_id' => $quiz_view_id,
                'score' => $score,
                'student_id' => $userId
            ]
        );

        $next_quiz = DB::table('quiz_view')
            ->select('quiz_view_id')
            ->where('syllabus_id', '=', $syllabus_id)
            ->where('quiz_view_id', '>', $quiz_view_id)
            ->first();

        $result = 0;
        if(count($next_quiz) != 0) $result = $next_quiz->quiz_view_id;

        //echo $next_quiz->quiz_view_id;

        $this->calculateScorePercentile($quiz_view_id);

        echo json_encode($result);

    }


    public function ranking() {
        student_ranking();
        if (checkStudentRanking(4, 10)) {
            echo "you are in top rank";
        }
    }


    public function calculateScorePercentile($quiz_view_id=1) {

        $results = DB::table('quiz_score')
            ->where('quiz_id', $quiz_view_id)
            ->orderBy('score','DESC')
            ->get();

        $p= count($results);
        $position = 1;

        foreach ($results as $result) {
            $l= $p - $position;
            if ($p != 0) $percentile = ($l/$p)*100;
            else $percentile = 100;
            $percentile = ceil($percentile);
            $position++;

            if (count($results) == 1) $percentile = 100;

            DB::table('quiz_score')
                ->where('quiz_score_id', $result->quiz_score_id)
                ->update([
                    'percentile' => $percentile
                ]);
            //echo $result->quiz_score_id . ' ======== ' . $result->score. '========' . $percentile ."<br>";
        }
    }


    public function viewQuizResult(Request $request) {

        $quiz_id = $request['quiz_id'];
        $student_id = Auth::user()->id;
        $this->calculateScorePercentile($quiz_id);

        $data = DB::table('quiz_score')
            ->select('quiz_score.*'
                , DB::raw("(SELECT unit_title FROM `syllabus` WHERE id= quiz_score.syllabus_id limit 1) as unit_title")
            )
            ->where('quiz_id', $quiz_id)
            ->where('student_id', $student_id)
            ->orderBy('created_at', 'desc')
            ->first();


        $next_quiz = DB::table('quiz_view')
            ->select('quiz_view_id')
            ->where('syllabus_id', '=', $data->syllabus_id)
            ->where('quiz_view_id', '>', $data->quiz_id)
            ->first();

        //$next_quiz = json_decode(json_encode($next_quiz), True);
        $next_quiz_id = 0;


        if (isset($next_quiz->quiz_view_id)) {
            $next_quiz_id = $next_quiz->quiz_view_id;
        }

        return view('4sages.quiz.view-quiz-result', [
            'data' => $data,
            'next_quiz_id' => $next_quiz_id,
        ]);
    }


    public function viewCourseResult($id) {

        $student_id = Auth::user()->id;

        $quiz_view = DB::table('quiz_view')
            ->select('quiz_view_id')
            ->where('syllabus_id', $id)
            ->get();

        $syllabus = DB::table('syllabus')
            ->where('id', $id)
            ->first();

        //$unit_title = $syllabus->unit_title;
        $analytics = array();

        foreach ($quiz_view as $quiz) {

            $quiz_score = DB::table('quiz_score')
                ->select('score')
                ->orderBy('score', 'desc')
                ->where('quiz_id', $quiz->quiz_view_id)
                ->where('student_id', $student_id)
                ->limit(1)
                ->first();

            array_push($analytics, $quiz_score->score);
        }

        return view('4sages.quiz.view-course-result', [
            'analytics' => $analytics,
            'syllabus' => $syllabus,
        ]);



    }


}
