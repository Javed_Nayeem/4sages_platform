<?php

define('Temp_user_validity', 7); // temporary user validity period in days

function call_create_course() {
    return View::make('4sages.course.createcourse')->render();
}


function call_add_syllabus($course_id) {
    return View::make('4sages.course.addsyllabus', ['course_id' => $course_id,])->render();
}


function create_directory() {

    $path = public_path() . '/course';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/course/thumbnail';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/course/preview';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/profile_photo';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/profile_photo/thumbnail';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/profile_photo/preview';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/profile_photo/small';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/content';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/flashcard';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/content/files';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/syllabus';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/syllabus/preview';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/questionimage';
    if (!File::exists($path)) File::makeDirectory($path);

    $path = public_path() . '/explanation';
    if (!File::exists($path)) File::makeDirectory($path);

}


function init() {
    $count = DB::table('users')->count();
    if ($count == 0) {
        DB::table('users')->insert([
            'name' => 'Javed Nayeem',
            'email' => 'admin@4sages.com',
            'password' => bcrypt('091288'),
            'role' => 'Admin'
        ]);
    }
}


function getName ($id) {
    $userName =  DB::table('users')->select('name')->where('id','=', $id)->first();
    return isset($userName->name) ? $userName->name: "Default Name";
}


function getCourseName ($id) {
    $courseName =  DB::table('courses')->select('course_name')->where('course_id','=', $id)->first();
    return isset($courseName->course_name) ? $courseName->course_name: "Default Name";
}


function is_youtube_link($link) {
    if (strpos($link, 'https://www.youtube.com') === 0 || strpos($link, 'https://youtu.be') === 0) return true;
    else return false;
}


function get_youtube_link($url) {
    $pos = strrpos($url , "=");
    return substr($url,++$pos);

}


function check_temp_user() {
    init();
    $date = date('Y-m-d H:i:s');
    $users =  DB::table('users')->select('id', 'created_at')->where('temporary_id','=', 1)->get();

    foreach ($users as $user) {
        if (dateDifference($user->created_at, $date) > Temp_user_validity) {
            DB::table('users')
                ->where('id',"=", $user->id)
                ->update([
                    'active' => 0
                ]);
        }
    }
}


function dateDifference($date_1 , $date_2 , $differenceFormat = '%d' ) {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);
}


function student_ranking() {

    $users = DB::table('users')
        ->select('id')
        //->where('role', '=', 'Student')
        ->get();

    $quizes = DB::table('quiz_view')
        ->select('quiz_view_id')
        ->get();


    foreach($users as $user) {
        //echo "<h3>Student id : " . $user->id . "</h3><br>";
        $student_id = $user->id;

        $total_score = 0;
        $quiz_count = 0;

        foreach ($quizes as $quiz) {
            //echo "<br>quiz id : " . $quiz->quiz_view_id . "<br>";

            $scores = DB::table('quiz_score')
                ->select('score')
                ->orderBy('score', 'desc')
                ->where('quiz_id', '=', $quiz->quiz_view_id)
                ->where('student_id', '=', $user->id)
                ->limit(1)
                ->get();

            foreach ($scores as $score) {
                //echo "score : " . $score->score . "<br>";
                $total_score += $score->score;
                $quiz_count++;
            }
        }


        $precision = 2;
        if ($quiz_count != 0) $percentile = ($total_score / ($quiz_count * 100.0)) * 100;
        else $percentile = 0;
        $percentile = substr(number_format($percentile, $precision+1, '.', ''), 0, -1);

        $student = DB::table('student_ranking')
            ->select('student_id')
            ->where('student_id', '=', $student_id)
            ->first();


        if ($quiz_count != 0) {

            if (count($student) == 0) {

                DB::table('student_ranking')->insert([
                        'quiz_count' => $quiz_count,
                        'score' => $total_score,
                        'percentage' => $percentile,
                        'student_id' => $student_id
                    ]
                );

            }
            else {
                $date = date('Y-m-d H:i:s');

                DB::table('student_ranking')
                    ->where('student_id', $student_id)
                    ->update([
                        'quiz_count' => $quiz_count,
                        'score' => $total_score,
                        'percentage' => $percentile,
                        'updated_at' => $date
                    ]);
            }

        }



        /*
                echo "total quiz count : " . $quiz_count . "<br>";
                echo "total score : " . $total_score . "<br>";
                echo "percentile : " . $percentile . "<br>";
        */

    }
    countPercentile();
}


function checkStudentRanking($student_id, $position) {

    $students = DB::table('student_ranking')
        ->select('student_id')
        ->orderBy('percentage','DESC')
        ->get();


    $percentile =  round(count($students) * ($position/100));
    $flag = false;

    for ($i=0; $i<$percentile; $i++) {
        if ($students[$i]->student_id == $student_id) {
            $flag = true;
            break;
        }
    }

    return $flag;

}


function calculatePercentile_1($user_id) {

    $students = DB::table('student_ranking')
        ->select('student_id')
        ->orderBy('percentage','DESC')
        ->get();

    $position = 0;
    $i = 0;

    foreach ($students as $student) {
        $i++;
        if ($student->student_id == $user_id) $position = $i;
    }

    $p= count($students);
    $l= $p - $position;
    if ($p != 0) $percentile = ($l/$p)*100;
    else $percentile = 100;
    $precision = 2;
    $percentile = substr(number_format($percentile, $precision+1, '.', ''), 0, -1);

    return $percentile;
}


function countPercentile() {

    $students = DB::table('student_ranking')
        ->select('student_id')
        ->orderBy('percentage','DESC')
        ->get();

    $p= count($students);
    $position= 1;
    $precision = 2;
    $date = date('Y-m-d H:i:s');

    foreach ($students as $student) {

        $l= $p - $position;
        $percentile = ($l/$p)*100;
        $percentile = substr(number_format($percentile, $precision+1, '.', ''), 0, -1);
        $position++;

        DB::table('student_ranking')
            ->where('student_id', $student->student_id)
            ->update([
                'percentile' => $percentile,
                'updated_at' => $date
            ]);
    }
}


function testFunction() {

    $myObj = array();
    $analytics = array();

    $student_id = 1;

    $syllabus = DB::table('syllabus')
        ->select('syllabus.id', 'syllabus.unit_title', 'courses.course_id', 'courses.course_name')
        ->rightJoin('courses', 'courses.course_id', '=', 'syllabus.course_id')
        ->where('syllabus.id', '>', 0)
        ->get();



    foreach ($syllabus as $syllabus) {
        //echo $syllabus->unit_title . " : " . $syllabus->course_name . "<br>";

        $quiz_view_id = DB::table('quiz_score')
            ->select('quiz_id', 'score')
            ->distinct('quiz_id')
            ->orderBy('score', 'desc')
            ->where('syllabus_id', '=', $syllabus->id)
            ->where('student_id', '=', $student_id)
            ->limit(1)
            ->get();

        foreach ($quiz_view_id as $quiz) {
            //echo $syllabus->unit_title . ' ========= ' . $quiz->score . '<br>';

            $myObj[0] = $syllabus->unit_title;
            $myObj[1] = $quiz->score;
            //$myJSON = json_encode($myObj);
            array_push($analytics, $myObj);
        }

    }

    echo "<pre>";
    var_dump($analytics);
    echo "<pre>";

    for ($i=0; $i<count($analytics); $i++) {
        echo $analytics[$i][0] . '<br>';
    }


}


function getSyllabusName($syllabus_id) {
    $syllabusName =  DB::table('syllabus')->select('unit_title')->where('id','=', $syllabus_id)->first();
    return isset($syllabusName->unit_title) ? $syllabusName->unit_title: "Default Name";
}
