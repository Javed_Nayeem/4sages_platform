<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration {

    public function up() {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('course_id');
            $table->string('course_name');
            $table->string('description')->nullable();
            $table->string('overview')->nullable();
            $table->string('topics')->nullable();
            $table->integer('tutor_id');
            $table->tinyInteger('course_status')->default(1);
            $table->string('image_title')->default('curriculum_default.jpg');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('courses');

    }
}
