<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseStudent extends Migration {

    public function up() {

        Schema::create('course_student', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('course_id');
            $table->integer('student_id');

            $table->timestamps();
        });

    }


    public function down() {

        Schema::dropIfExists('course_student');

    }
}
