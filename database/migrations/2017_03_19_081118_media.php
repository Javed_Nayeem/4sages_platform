<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media extends Migration {

    public function up() {

        Schema::create('media', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('author_id');
            $table->string('post_title');
            $table->string('post_mime_type');
            $table->integer('duration')->default(0);
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
            $table->timestamps();

        });

    }


    public function down() {

        Schema::dropIfExists('media');

    }
}
