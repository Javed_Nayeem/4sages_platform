<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SyllabusMessage extends Migration {

    public function up() {

        Schema::create('syllabus_message', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('syllabus_id');
            $table->integer('content_id');
            $table->integer('start');
            $table->integer('end');
            $table->string('question');
            $table->string('option_1');
            $table->string('option_2');
            $table->string('option_3');
            $table->string('answer');
            $table->integer('pause')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

    }


    public function down() {

        Schema::dropIfExists('syllabus_content');

    }
}
