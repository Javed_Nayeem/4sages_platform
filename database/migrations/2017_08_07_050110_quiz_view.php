<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizView extends Migration {

    public function up() {
        Schema::create('quiz_view', function (Blueprint $table) {
            $table->increments('quiz_view_id');
            $table->integer('syllabus_id');
            $table->integer('course_topic_id');
            $table->integer('tutor_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('quiz_view');
    }
}
