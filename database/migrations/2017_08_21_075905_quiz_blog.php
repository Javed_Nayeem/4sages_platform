<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuizBlog extends Migration {
    public function up() {

        Schema::create('quiz_blog', function (Blueprint $table) {
            $table->increments('quiz_blog_id');
            $table->integer('quiz_view_id');
            $table->string('blog_name')->nullable();
            $table->string('blog_link');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('quiz_blog');
    }
}
