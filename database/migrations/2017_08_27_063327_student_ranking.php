<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentRanking extends Migration {

    public function up() {
        Schema::create('student_ranking', function (Blueprint $table) {
            $table->increments('rank_id');
            $table->integer('quiz_count');
            $table->integer('score');
            $table->double('percentage')->nullable();
            $table->double('percentile')->nullable();
            $table->integer('student_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('student_ranking');
    }
}
