<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Activity extends Migration {

    public function up() {
        Schema::create('activity', function (Blueprint $table) {
            $table->increments('activity_id');
            $table->integer('user_id');
            $table->integer('activity_description');
            $table->integer('curriculum_id')->nullable();
            $table->integer('syllabus_id')->nullable();
            $table->integer('quiz_id')->nullable();
            $table->integer('student_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('activity');
    }
}
