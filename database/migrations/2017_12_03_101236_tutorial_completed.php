<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TutorialCompleted extends Migration {

    public function up() {
        Schema::create('tutorial_completed', function (Blueprint $table) {
            $table->increments('tutorial_completed_id');
            $table->integer('student_id');
            $table->integer('syllabus_id');
            $table->double('completed_percentage');
            $table->integer('c_time');
            $table->timestamp('created_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('tutorial_completed');
    }
}
