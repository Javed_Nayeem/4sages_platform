var room = 1;
function education_fields() {

    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+room);
    var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><label>Question</label><input type="text" class="form-control" name="question[]" value="" placeholder="Question"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><label>Option 1</label><input type="text" class="form-control" name="option[]" value="" placeholder="Option 1"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><label >Option 2</label><input type="text" class="form-control" name="option[]" value="" placeholder="Option 2"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group"> <label>Answer</label><input type="text" class="form-control" name="answer[]" value="" placeholder="Answer"><div class="input-group-btn"> <button class="btn btn-danger add_question_button" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

    objTo.appendChild(divtest)
}


function remove_education_fields(rid) {
    $('.removeclass'+rid).remove();
}


$(document).ready(function() {

    $("#question_save").click(function(){

        var quiz_title = $("#quiz_title").val();
        var course_id = $("#course_id").val();
        var flag = false;
        var question_array = [];
        var option_array = [];
        var answer_array = [];


        $('input[name^="question"]').each(function() {

            if ($(this).val() == "") flag = true;
            else question_array.push($(this).val());
        });

        //console.log("========================");

        $('input[name^="option"]').each(function() {
            if ($(this).val() == "") flag = true;
            else option_array.push($(this).val());

        });

        $('input[name^="answer"]').each(function() {

            if ($(this).val() == "") flag = true;
            else answer_array.push($(this).val());

        });
        console.log(answer_array);

        if (flag) alert("field empty");

        else {

            var params = {
                quiz_title: quiz_title,
                course_id: course_id,
                question_array: question_array,
                option_array: option_array,
                answer_array:answer_array
            };

            $.ajax({
                url: '/add/quiz',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    console.log(response);

                    var htmlStr = '<div class="col-lg-4 col-md-4 col-sm-6"><div class="lms_video"><div></div><div class="lms_hover_section"> <img src="/images/courses/quiz.jpg" alt="quiz_image"><div class="lms_hover_overlay"><a class="lms_image_link"></a></div></div><a href="/course/quiz/'+response+'"><h3>Quiz : '+quiz_title+'<br></div></div>';

                    $('#quiz_list').append(htmlStr);



                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }



    });


    $("#course_update").click(function(){

        var course_name = $("#course_name").val();
        var description = $("#description").val();
        var course_overview = $("#course_overview").val();
        var course_id = $("#course_id").val();
        if(course_name == "" || description == "" || course_overview == ""){
            Alert ("Please fill up the gaps first !!!");
        }
        else{
            var params = {
                course_name: course_name,
                description: description,
                course_overview: course_overview,
                course_id: course_id
            };


            $.ajax({
                url: '/editcourse',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},  //send these values to controller

                success: function (response) {
                    console.log(response);
                    $("#index_course_name").text(course_name);
                    $("#index_course_description").text(description);
                    $("#index_course_overview").text(course_overview);


                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


    $("#course_delete").click(function(){

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {

            var course_id = $("#course_id").val();

            var params = {
                course_id: course_id,
            };

            $.ajax({
                url: '/course/delete/',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal({
                        type: 'success',
                        html: $('<div>')
                            .addClass('some-class')
                            .text('Curriculum has been deleted'),
                        animation: false,
                        customClass: 'animated bounceIn'
                    });

                    setTimeout(function(){
                        window.location.href = "/courselist";
                    },2000);
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }, function (dismiss) {
            if (dismiss === 'cancel') {
                swal({
                    title: 'Cancelled',
                    type: 'error',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Your Curriculum is safe :)'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            }
        });


    });


    $("#send_forum_message").on('keyup', function (e) {
        if (e.keyCode == 13) {

            var message = $("#send_forum_message").val();
            var course_id = $("#course_id").val();

            var params = {
                message: message,
                course_id: course_id
            };

            $.ajax({
                url: '/course/sendmessage',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    console.log(response);
                    $("#send_forum_message").val("");

                    var htmlStr = '<li><img src="/images/online/member1.jpg" alt=""/><span>'+message+'</span></li>';
                    $('#forum_table').append(htmlStr);

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }
    });


    $("#add_student_button").click(function () {

        var student_list = [];
        var course_id = $("#course_id").val();

        $('#add_student_table').find('input[type="checkbox"]:checked').each(function () {
            student_list.push(this.value);
        });

        var params = {
            student_list: student_list,
            course_id: course_id
        };

        //console.log(params);

        $.ajax({
            url: '/course/addstudent',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                console.log(response);
                $("#message_display").text("Student Added");
                $("#student_count").text(response);
                $( ".online" ).remove();

                var htmlStr = '';

                for (var i=0; i<response; i++) {
                    htmlStr += '<li class="online"><a href=""><img src="/images/online/student1.jpg" alt="online' +
                        ' member"></a></li>';
                }


                $('#online_student_list').append(htmlStr);


            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });


    $("#course_invite").click(function () {

        var course_id = $("#course_id").val();
        var email = $("#invite_email").val();

        var params = {
            course_id: course_id,
            email: email
        };

        $.ajax({
            url: '/course/invite',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                console.log(response);
                htmlStr = '<li>'+email+' &nbsp; &nbsp; &nbsp; Status : pending</li>';
                $('.lms_underlist_doted').append(htmlStr);
                swal("Invitation Sent", "Invitation has been sent to " + email, "success")
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });


    });

});




