
var question_count = 1;
var correct_ans = [];
var limit_of_question = 10;

$(document).ready(function(){

    $("#add_new_question").click(function(){

        question_count++;

        if (question_count > limit_of_question) {
            swal("Limit Exceed", "Maximum number of question is 10", "error");
            //$("#add_new_question").attr("disabled", true);
        }

        else {

            var htmlStr = '<div class="col-lg-12 text-center top-margin-50" id="question_'+ question_count +'"><div class="row"><div class="form-group"><label class="col-md-2 control-label">Question '+ question_count +'</label><div class="col-md-8 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><textarea class="form-control" name="question[]" placeholder="Write your question here" rows="3" cols="200" required></textarea></div></div></div></div><div class="row top-margin-10"><div class="form-group"><label class="col-md-2 control-label">Option 1</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" name="option[]" placeholder="Option 1" required></div></div></div></div><div class="row top-margin-10"><div class="form-group"><label class="col-md-2 control-label">Option 2</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" name="option[]" placeholder="Option 2" required></div></div></div></div><div class="row top-margin-10"><div class="form-group"><label class="col-md-2 control-label">Option 3</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" name="option[]" placeholder="Option 3" required></div></div></div></div><div class="row top-margin-10"><div class="form-group"><label class="col-md-2 control-label">Correct Answer</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><input type="text" class="form-control" name="answer[]" placeholder="Correct Answer" required></div></div></div></div><div class="row top-margin-10"><div class="form-group"><label class="col-md-2 control-label">Media</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="fa fa-paperclip"></i></span><input type="file" class="form-control" name="media_'+ question_count +'" value="default" accept="image/*"></div></div></div></div><div class="row" style="margin-top: 10px"><div class="form-group"><label class="col-md-2 control-label">Explanation</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span><textarea class="form-control" name="explanation[]" placeholder="Explanation for this question answer" rows="3" cols="200" required></textarea></div></div></div></div><div class="row" style="margin-top: 10px"><div class="form-group"><label class="col-md-2 control-label">Explanation Media</label><div class="col-md-3 inputGroupContainer"><div class="input-group"><span class="input-group-addon"><i class="fa fa-paperclip"></i></span><input type="file" class="form-control" name="explanation_media_'+ question_count +'" value="default" accept="image/*, video/*"></div></div></div></div></div>';

            $('#question_table').append(htmlStr);

        }
    });


    $("#remove_question").click(function(){
        if (question_count > 1) {
            $( "#question_" + question_count ).remove();
            question_count--;
        }

    });


    $("#add_new_blog").click(function(){

        var new_blog_name = $("#new_blog_name").val();
        var new_blog_link = $("#new_blog_link").val();

        if (new_blog_link != "") {

            var htmlStr = '<input type="hidden" name="blog_name[]" value="'+ new_blog_name +'">';
            htmlStr += '<input type="hidden" name="blog_link[]" value="'+ new_blog_link +'">';
            if (new_blog_name == "") htmlStr += '<li>'+ new_blog_link +'</li>';
            else htmlStr += '<li>'+new_blog_name + ': ' + new_blog_link +'</li>';

            $('#blog_list').show();
            $('#no_blog_text').hide();
            $('#blog_list').append(htmlStr);

        }

        else {
            swal("Blog Link", "Provide Valid Blog Link", "warning");
        }

        $("#new_blog_name").val("");
        $("#new_blog_link").val("");


    });


    $("#submit_quiz_button").click(function(){

        var submitted_ans = [];
        /*
         $('.magic-radio:checked').each(function(j){
         submitted_ans.push($(this).val());
         });
         */
        var i = 0;
        var temp_value = "";

        $('.magic-radio').each(function () {
            if ($(this).is(':checked')) {
                temp_value = $(this).val();
                //console.log("checked value : " + $(this).val());
            }

            if (i%3 == 2) {
                //console.log("value : " + temp_value);
                submitted_ans.push(temp_value);
                temp_value = "";
            }
            i++;
        });


        var count = 0;
        for (var i = 0; i < submitted_ans.length; i++) {
            if (correct_ans[i] === submitted_ans[i]) count++;
            else {
                $("#explanation_div_" + (i+1)).show();
                $("#answer_div_" + (i+1)).text("Correct Ans :" + correct_ans[i]);
                $(".i_correct").show();
            }
        }
        var score = (count / submitted_ans.length) * 100;
        score = score.toPrecision(3);
        var message_1 = "You scored " + score + "%";
        var message_2 = "";
        if (score == 0.00) {
            message_1 = "You scored 0%";
            message_2 = "Come on! You can do better.";
            score = 0;
        }
        swal(message_1, message_2, "success");
        document.getElementById("submit_quiz_button").disabled = true;

        $('#message').show();
        $('#back_button').show();
        $('#blog_list_div').show();
        $("#view_results").show();

        var quiz_view_id = $("#quiz_view_id").val();
        var syllabus_id = $("#syllabus_id").val();

        var params = {
            quiz_view_id: quiz_view_id,
            score: score,
            syllabus_id: syllabus_id,
        };

        $.ajax({
            url: '/store/result',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                if (response != 0) {
                    $("#course_quiz").val(response);
                    $("#another_quiz").show();
                }
                //console.log("next quiz id :" + response);
                var a = document.getElementById('back_button');
                a.href = "/course/tutorial/" + syllabus_id;

                $("#back_button").show();

                // setTimeout(function(){
                //     window.location.href = "/course/tutorial/" + syllabus_id;
                // },2000);
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });


    $("#another_quiz").click(function(){
        document.getElementById("quiz_form").submit();
    });


    $('#view_results').click(function () {
        document.getElementById("quiz_result_form").submit();
    });

});


function store_ans(ans) {
    correct_ans.push(ans);
}


function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

