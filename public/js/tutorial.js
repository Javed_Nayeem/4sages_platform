var video;


$(document).ready(function(){
    var syllabus_id = $("#syllabus_id").val();
    video = $('video')[0];
    var repeater;
    var count = 0;
    $("#question_modal_close_button").hide();


    // $('#testing_purpose').addClass('animated flash');

    //console.log("%cExtra Large Yellow Text with Red Background", "background: red; color: yellow; font-size: x-large");




    //var question_div_flag = false;

    var messageArray = [];
    var answerArray = [];
    var studentAnswerArray = [];

    $.ajax({
        url: '/get/message/' + syllabus_id,
        type: 'POST',
        data: {"_token": $('#token').val()},

        success: function (response) {
            //console.log("printing message from post");
            var json_message = $.parseJSON(response);
            //console.log(response);
            for (var i=0; i<json_message.length; i++) {
                messageArray.push({
                    start: json_message[i].start,
                    end: json_message[i].end,
                    question: json_message[i].question,
                    option_1: json_message[i].option_1,
                    option_2: json_message[i].option_2,
                    option_3: json_message[i].option_3,
                    pause: json_message[i].pause,
                    show: 1
                });
                answerArray.push(json_message[i].answer);
            }



        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });


    function doWork() {
        showMessage(messageArray);
        repeater = setTimeout(doWork, 1000);
    }
    doWork();


    function create_animation() {
        var div = $("#test_div");
        div.animate({'marginLeft' : '+=700px'}, 10000);
        div.animate({'marginLeft' : '-=700px'}, 10000);
        repeater = setTimeout(create_animation, 10);
    }
    create_animation();


    $("#tutorial_ans_submit").click(function(){

        $("#tutorial_ans_submit").hide();
        $("#question_modal_close_button").show();

        var given_answer = $('input[name=student_answer]:checked').val();
        //console.log("answer submitted : " + given_answer);
        studentAnswerArray.push(given_answer);

        var last_count = studentAnswerArray.length - 1;

        $("#question_div_message").show();
        if (answerArray[last_count] === studentAnswerArray[last_count]) {
            $( "#message_output_feedback" ).text("Awesome!");
            // $( "#message_output_feedback" ).css('color', '#1abc9c');
            $( "#message_output_feedback_2" ).text("");
        }
        else {
            $( "#message_output_feedback" ).text("Oops!");
            // $( "#message_output_feedback" ).css('color', '#b30000');

            $( "#message_output_feedback_2" ).text("Correct Answer is: " + answerArray[last_count]);
            // $( "#message_output_feedback_2" ).css('color', '#2980b9');

        }

        $("#question_div").hide();
        //setCurTime(getCurTime()+1);
        setTimeout(function(){
            $('#question_modal').modal('hide');
            $("#question_modal_close_button").hide();
            video.play();
        },3000);

        //video.play();
        //question_div_flag = false;
        //$( "#question_div" ).remove();
    });






    $("#q1_option_1").bind("keyup change", function(e) {
        var option = $('#q1_option_1').val();
        $("#q1_ans_1").text(option);
        $("#q1_ans_1").val(option);
    });

    $("#q1_option_2").bind("keyup change", function(e) {
        var option = $('#q1_option_2').val();
        $("#q1_ans_2").text(option);
        $("#q1_ans_2").val(option);
    });

    $("#q1_option_3").bind("keyup change", function(e) {
        var option = $('#q1_option_3').val();
        $("#q1_ans_3").text(option);
        $("#q1_ans_3").val(option);
    });




    $("#q2_option_1").bind("keyup change", function(e) {
        var option = $('#q2_option_1').val();
        $("#q2_ans_1").text(option);
        $("#q2_ans_1").val(option);
    });

    $("#q2_option_2").bind("keyup change", function(e) {
        var option = $('#q2_option_2').val();
        $("#q2_ans_2").text(option);
        $("#q2_ans_2").val(option);
    });

    $("#q2_option_3").bind("keyup change", function(e) {
        var option = $('#q2_option_3').val();
        $("#q2_ans_3").text(option);
        $("#q2_ans_3").val(option);
    });




    $("#q3_option_1").bind("keyup change", function(e) {
        var option = $('#q3_option_1').val();
        $("#q3_ans_1").text(option);
        $("#q3_ans_1").val(option);
    });

    $("#q3_option_2").bind("keyup change", function(e) {
        var option = $('#q3_option_2').val();
        $("#q3_ans_2").text(option);
        $("#q3_ans_2").val(option);
    });

    $("#q3_option_3").bind("keyup change", function(e) {
        var option = $('#q3_option_3').val();
        $("#q3_ans_3").text(option);
        $("#q3_ans_3").val(option);
    });

    $("video").on("pause", function (e) {

        var vid = document.getElementById("video_content");
        var total_duration = vid.duration;
        var current_time = e.target.currentTime;

        var completed_percentage = Math.ceil(((current_time * 100.0) / total_duration));
        //console.log('completed percentage : ' + completed_percentage);

        var params = {
            syllabus_id: $('#syllabus_id').val(),
            completed_percentage: completed_percentage,
            c_time: $('#c_time').val()
        };

        $.ajax({
            url: '/tutorial/complete',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {

            },
            error: function (error) {

            }
        });

    });


    $('video').on('ended',function(){

        var params = {
            syllabus_id: $('#syllabus_id').val(),
            completed_percentage: 100,
            c_time: $('#c_time').val()
        };

        $.ajax({
            url: '/tutorial/complete',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {

            },
            error: function (error) {

            }
        });

        var count = 0;

        for (var i = 0; i < studentAnswerArray.length; i++) {
            if (answerArray[i] === studentAnswerArray[i]) {
                count++;
            }
        }

        var un_answered = answerArray.length - studentAnswerArray.length;

        var score = (count / answerArray.length) * 100;
        score = score.toPrecision(3);

        var message_1 = "You scored " + score + "%";
        //var message_2 = "You answered " + count + " questions out of " + answerArray.length + " questions";
        var message_2 = "";

        if (score == 0.00) {
            message_1 = "You scored 0%";
            message_2 = "Come on! You can do better.";
        }


        if (score != 'NaN') swal(message_1, message_2, "success");


        studentAnswerArray = [];

        //var vid = document.getElementById("myVideo");
        //vid.currentTime=5;

        // var vid = document.getElementById("video_content");
        // vid.currentTime=5;
        //$('#video_content').attr('currentTime', 5);

        $( "#message_output_feedback" ).text("");
        $( "#message_output_feedback_2" ).text("");



    });



});

function log(video_top) {
    $("#question_modal_top").css('top', video_top-10 + "px");


    var video_width = $("#video_content").width();
    video_width = (video_width * 0.95);
    $("#question_modal_top").css("width", video_width);

    var video_height = $("#video_content").height();
    video_height = (video_height * 0.95);
    $("#question_modal_top").css("height", video_height);


    //console.log("location : " + video_top + "px");
    //console.log("width : " + video_width + "px");
}


$(function() {
    var eTop = $('#video_content').offset().top; //get the offset top of the element
    log(eTop - $(window).scrollTop()); //position of the ele w.r.t window

    $(window).scroll(function() { //when window is scrolled
        log(eTop - $(window).scrollTop());
    });
});


function getCurTime() {
    return parseInt(video.currentTime);
}

function setCurTime(time) {
    video.currentTime = time;
}

function showMessage(messageArray) {

    for (var i=0; i<messageArray.length; i++) {
        if (messageArray[i].start == getCurTime()) {


            if (messageArray[i].show == 1) {



                $("#question_div_message").hide();
                if (messageArray[i].pause == 1) {
                    video.pause();
                    //messageArray[i].pause = 0;
                }

                $('input:radio[name=student_answer]').each(function () { $(this).prop('checked', false); });

                //video.webkitExitFullScreen();


                $('#question_modal').modal('show');
                $('#question_modal_close_button').modal('hide');
                $("#tutorial_ans_submit").show();


                $("#question_div").show();
                $("#message_output").text(messageArray[i].question);

                $("#option1").text(messageArray[i].option_1);
                $("#option2").text(messageArray[i].option_2);
                $("#option3").text(messageArray[i].option_3);

                $("#question_div_o1").val(messageArray[i].option_1);
                $("#question_div_o2").val(messageArray[i].option_2);
                $("#question_div_o3").val(messageArray[i].option_3);
                messageArray[i].show = 0;
            }


        }
        //else $("#question_div").hide();
    }
}

function flashcard_view(title, description, image) {

    $("#fc_title").text(title);
    $("#fc_description").text(description);
    $('#fc_image').attr('src','/flashcard/'+ image);

    $('#flash_card_modal').modal('show');

}

function quiz(id) {

    $("#course_quiz").val(id);
    document.getElementById("quiz_form").submit();

}
