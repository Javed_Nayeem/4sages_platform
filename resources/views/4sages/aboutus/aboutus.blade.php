@extends('layouts.header')
@section('title', 'What we Do')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-lg-8 col-lg-offset-2">
          <div class="lms_title_center">
            <div class="lms_heading_1">
              <h2 class="lms_heading_title">About Us</h2>
            </div>
            <p>Aliquam et venenatis arcu, ac dictum felis. Nulla sapien dolor, tempor sodales mauris et, ultrices tique odio.</p>
          </div>
        </div>
      </div>
      
   <div class="row">
   	  <div class="col-lg-4 col-md-4 col-sm-4">
      	<div class="lms_service_3">
            	<div class="lms_service_icon"><img src="images/services/services1.png" alt="service icon"></div>
                <a href=""><h3 class="lms_service_title">Learn Anything Online</h3></a>
                <p>Lorem ipsum dolor sit amet, consectetur adDonec idiam dapibus, sodales odio quis, fgilla maurisorci. Nullam vel laoreet dui. Sed blandit quis sapien sed, </p>
            </div>
      </div>
      
      <div class="col-lg-4 col-md-4 col-sm-4">
      	<div class="lms_service_3">
            	<div class="lms_service_icon"><img src="images/services/services2.png" alt="service icon"></div>
                <a href=""><h3 class="lms_service_title">Talk to Our Expertse</h3></a>
                <p>Lorem ipsum dolor sit amet, consectetur adDonec idiam dapibus, sodales odio quis, fgilla maurisorci. Nullam vel laoreet dui. Sed blandit quis sapien sed, </p>
            </div>
      </div>
      
      <div class="col-lg-4 col-md-4 col-sm-4">
      	<div class="lms_service_3">
            	<div class="lms_service_icon"><img src="images/services/services3.png" alt="service icon"></div>
                <a href=""><h3 class="lms_service_title">Communicate with People</h3></a>
                <p>Lorem ipsum dolor sit amet, consectetur adDonec idiam dapibus, sodales odio quis, fgilla maurisorci. Nullam vel laoreet dui. Sed blandit quis sapien sed, </p>
            </div>
      </div>
      
   		</div>   
<!--Our Recent Achievements start-->
   <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="lms_title_center">
            <div class="lms_heading_1">
              <h2 class="lms_heading_title">Our Recent Achievements</h2>
            </div>
            <p>Aliquam et venenatis arcu, ac dictum felis. Nulla sapien dolor, tempor sodales mauris et, ultrices tique odio. </p>
          </div>
        </div>
      </div>
   
   <div class="row">
   	  <div class="col-lg-4 col-md-4 col-sm-4">
      	  <div class="lms_recent_achievement">
          	  <img src="images/other/recent_achievement1.jpg" alt="recent achievement" />
              <h3>Suspendisse eget tortor </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus egestas augue ac nisi pharegittis.</p>
          </div>	
      </div>
      
      <div class="col-lg-4 col-md-4 col-sm-4">
      	  <div class="lms_recent_achievement">
          	  <img src="images/other/recent_achievement2.jpg" alt="recent achievement" />
              <h3>Suspendisse eget tortor </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus egestas augue ac nisi pharegittis.</p>
          </div>	
      </div>
      
      <div class="col-lg-4 col-md-4 col-sm-4">
      	  <div class="lms_recent_achievement">
          	  <img src="images/other/recent_achievement3.jpg" alt="recent achievement" />
              <h3>Suspendisse eget tortor </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus egestas augue ac nisi pharegittis.</p>
          </div>	
      </div>
   </div> 
   <!--Our Recent Achievements end-->   

</div>
 																<!-- main container ends  -->


<div class="container-fluid">
  <div class="row">	
    <div class="lms_our_success">
    	<div class="lms_our_success_bg" data-stellar-background-ratio="0.5">
        <div class="lms_our_success_bg_overlay">
        	<div class="container">
               <!--Our Success in Numbers start-->
               <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <div class="lms_our_success_title wow fadeInDown">
                   	  <h3>Our Success in Numbers</h3>
                      <p>Aliquam et venenatis arcu, ac dictum felis. Nulla sapien dolor, tempor sodales mauris et,</p>
                    </div>  
                   </div> 
               </div>
               
               <div class="row">
               	<div class="col-lg-3 col-md-3 col-sm-3 wow fadeInLeft">
                	<div class="lms_count">
                    	<i class="fa fa-user"></i>
                    	<div class="lms_progress_value count appear-count"  data-from="0" data-to="2848" data-speed="2000" data-refresh-interval="50"></div>
                   <p class="lms_progess_label"> student </p>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-3 wow fadeInDown">
                	<div class="lms_count">
                    	<i class="fa fa-book"></i>
                    	<div class="lms_progress_value count appear-count"  data-from="0" data-to="1048" data-speed="2000" data-refresh-interval="50"></div>
                   <p class="lms_progess_label"> courses </p>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-3 wow fadeInDown">
                	<div class="lms_count">
                    	<i class="fa fa-male"></i>
                    	<div class="lms_progress_value count appear-count"  data-from="0" data-to="2866" data-speed="2000" data-refresh-interval="50"></div>
                   <p class="lms_progess_label"> visitor </p>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-3 wow fadeInRight">
                	<div class="lms_count">
                    	<i class="fa fa-graduation-cap"></i>
                    	<div class="lms_progress_value count appear-count"  data-from="0" data-to="1005" data-speed="2000" data-refresh-interval="50"></div>
                   <p class="lms_progess_label"> experts </p>
                    </div>
                </div>
              </div>
               
               <!--Our Success in Numbers end-->
            </div>
        </div>
       </div> 
    </div>
  </div>
</div>


<div class="container">
	<div class="row">
    	<div class="col-lg-8 col-lg-offset-2">
          <div class="lms_title_center">
            <div class="lms_heading_1">
              <h2 class="lms_heading_title">About our Organization</h2>
            </div>
            <p>Aliquam et venenatis arcu, ac dictum felis. Nulla sapien dolor, tempor sodales mauris cum ne periculis molestiae pri.</p>
          </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-lg-6 col-md-6 col-sm-6">
        	<h3>Who We Are</h3>
            <p>Lorem ipsum dolor sit amet, ea doming epicuri iudicabit nam, te usu virtute placerat. Purto brute disputando cu est, eam dicam soluta ei. Vel dicam vivendo accusata ei, cum ne periculis molestiae pri. </p>
            
            <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Mauris posuere</a> </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body"> Lorem ipsum dolor sit amet, ea doming epicuri iudicabit nam, te usu virtute placerat. Purto brute disputando cu est, eam dicam soluta ei. Vel dicam vivendo accusata ei, cum ne periculis molestiae pri. 
            
            <ul class="lms_underlist_doted">
            	<li>Animating the lower and upper body, arms, and legs</li>
                <li>Adding weight</li>
                <li>Adjusting timing and gait</li>
            </ul>
            


            
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Aliquam et venenatis</a> </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Nulla nec tristique </a> </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
          </div>
        </div>
      </div>
            
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
        	<h3>Our Skills</h3>
            <p>Lorem ipsum dolor sit amet, ea doming epicuri iudicabit nam, te usu virtute placerat. Purto brute disputando cu est, eam dicam soluta ei. Vel dicam vivendo accusata ei, cum ne periculis molestiae pri.</p> 
<br/>
<p>Aliquam et venenatis arcu, ac dictum felis. Nulla sapien dolor, tempor sodales mauris et, ultrices tristique odio. </p>
            
            
            <div class="lms_flat_skills">
            	<label>Photoshop</label>
              <div class="progress">
                <div class="progress-bar lms_skill_photoshop" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:90%;"> 90% </div>
              </div>
              <label>Illustrator</label>
              <div class="progress">
                <div class="progress-bar lms_skill_illustrator" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:80%;"> 80% </div>
              </div>
              <label>HTML / CSS</label>
              <div class="progress">
                <div class="progress-bar lms_skill_html" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:90%;"> 90% </div>
              </div>
              <label>PHP / MYSQL</label>
              <div class="progress">
                <div class="progress-bar lms_skill_php" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:70%;"> 70% </div>
              </div>
            </div>
            
            
        </div>
        
    </div>
    
    
</div>
@endsection
