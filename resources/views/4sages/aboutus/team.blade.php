@extends('layouts.header')
@section('title', 'Team')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="lms_title_center">
                    <div class="lms_heading_1">
                        <h2 class="lms_heading_title">Our team</h2>
                    </div>
                    <p>Duis tortor lacus, dictum nec augue a, euismod sagittis eros. Aliquam id ligula
                        malesuada, egestas est ultricies, ullamcorper diam.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/Arif_4Sages_1.jpeg" alt="CEO"/>
                </div>
            </div>

            <div class="col-lg-7">
                <div class="lms_team_single_detail">
                    <h3>Muhammad Arif, Ph.D.</h3>
                    <p>Trained as a PhD scientist, I have worked with remarkably talented teams conducting life sciences research at Harvard, MIT and the University of New Hampshire. However, when I looked outside of such demographic niches, I frequently witnessed the disconnect between scholarly ambitions and learning outcomes. I believe the definition of "all-American" needs to include (and popularize) the idea that being scholarly is being cool. And it needs to resonate with students at an EARLY age. Middle school is where scholars are made.</p>


                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-02 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/Fahima_4Sages_1.jpeg" alt="CTO" />
                </div>
            </div>

            <div class="col-lg-7">
                <div class="lms_team_single_detail">
                    <h3>Fahima Islam</h3>
                    <p>After completing a Master’s degree involving scanning and transmission electron microscopy experiments, my academic interest gravitated toward database technology, data analysis and digital marketing automation. I now have 10 years of extensive leadership experience with technology strategy formulation and implementation. At the end of 2015 I decided to focus on the entrepreneurial passion in education technology and knowledge visualization. So, these days I am immersed in emerging tech trends that can potentially inspire student engagement and enhance learning outcomes.</p>



                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-02 -->

        <!-- Member no-03 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/team1.jpg" alt="team single" height="400" width="100"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Simom Hasan</h2>
                    <h3>Manager, Operations</h3>
                    <p>
                        Mr. Hasan has cumulative eight (8) years of experience in the field of Project Management, Business communication, marketing communication and IT project managements. Mr. Hasan is working as Operations Managers for managing overall activities of Nulytics (BD).<br>

                        Mr. Hasan is also experienced in 2D/3D animation, computer graphics works with different software products like Adobe Creative Suite, 3D Studio Max and other related applications. Previously, Mr. Hasan was the IT Manager of Edward M Kennedy Center managed by US Embassy. He was the Assistant Project Manager of Automation project of Forensic DNA Laboratory of Bangladesh Police.<br>

                        During his career in IT, Mr. Hasan completed his PGD in IT from IIT, Dhaka University
                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-03 -->

        <!-- Member no-04 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/team1.jpg" alt="team single" height="400" width="100"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Munsur Rahman</h2>
                    <h3>Senior CG Artist</h3>
                    <p>
                        With ten (10) years of cumulative experiences as Graphics designer on different renowned organizations, Mr. Rahman is now Senior CG Artist at Nulytics (BD). Mr. Rahman heads the 3D animation team of Nulytics (BD) for managing all 3D animation aspects of diverse varieties from the smaller to complex projects.<br>

                        Mr. Rahman is also a registered government trainer for National Institute of Mass Communication (NIMCO) and National Academy for Planning and Development (NAPD). After completing his Bachelor of Commerce under National University, Mr. Rahman did his Diploma in Computer Multimedia from National Youth Development Academy and from Ananda Multimedia Dhaka.<br>

                        He worked at Arean Phone BD Limited, Crossline and Armass Bangladesh as Graphics Designer and 3D artist. Mr. Rahman is well versatile with computer application like 3D Studio Max, After Effects, Sound Forge, Adobe Audition and all Adobe graphics designing applications.<br>


                        During his career in IT, Mr. Hasan completed his PGD in IT from IIT, Dhaka University <br>
                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-04 -->
        <!-- Member no-05 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/team1.jpg" alt="team single" height="400" width="100"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Quedrat-E-Noor</h2>
                    <h3>CG Artist</h3>
                    <p>
                        With nine (9) years of cumulative experiences as Graphics Designer, Assistant Director for at least 6-7 documentary films, TV drama and telefilms, Mr. Noor is now working with Nulytics (BD) as CG artist. <br>

                        Since 2010, Mr. Noor has been fully focused on 2D animation, 3D animation and motion graphics projects. He worked as Assistant Graphics Designer at Channel I for 3 years. Mr. Noor manages 2D animation and motion graphics team of Nulytics (BD).<br>

                        Education and knowledge delivery platforms of medium to large magnitude requiring motion graphics and planning, execution and delivery on time have been a constant requirement that have been fulfilled by Mr. Noor at Nulytics (BD)<br>
                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-05 -->
        <!-- Member no-06 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/team1.jpg" alt="team single" height="400" width="100"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Dilip Talukder</h2>
                    <h3>Artist</h3>
                    <p>
                        With thirty (30) years of experience as 2D drawing instructor, illustration of different curriculum books, Mr. Talukder is working with Nulytics (BD) as Senior Artist for all types of real life drawing and digitization projects to facilitate 2D animation.<br>

                        Mr. Talukder’s fame as a painter in classical techniques is greatly valued at Nulytics (BD) where he brings a traditional perspective into aesthetics and drawing skills. Knowledge delivery and information dissemination are not all just computer-driven and software-specific automatic processes. <br>

                        We value his contribution in understanding when a picture is worth more than a thousand words. His innovative approaches of using modern technologies like iPAD with stylus pen for drawing also helps the 2D and 3D animation team better understand concepts, contexts and nuances of the art.<br>


                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-06 -->
        <!-- Member no-07 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/JavedNayeem.jpg" alt="team single" style="border-radius: 5px"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Javed Nayeem</h2>
                    <h3>Senior Programmer</h3>
                    <p>Javed has cumulative eight (8) years of experience in the field of Programming, Application Development and IT project managements.<br>During his career in IT, Javed is doing his Master's in Computer Science & Engineering from Jahangirnagar University


                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-07 --><!-- Member no-07 -->
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="lms_team_single_img">
                    <img src="images/team/team2.jpg" alt="team single" height="400" width="100"/>
                </div>
            </div>

            <div class="col-lg-7" style ="margin-top: 50px;">
                <div class="lms_team_single_detail">
                    <h2>Ireen Mahmood</h2>
                    <h3>Programmer</h3>
                    <p>
                        Mauris finibus, nisi ut gravida commodo, nulla orci venenatis metus, sit amet rutrum nulla tellus nec mauris. Aenean turpis tortor, bibendum ut nulla a posuere.<br>

                        Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.<br>

                        Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus.<br>


                    </p>

                    <h4>Connect with Us</h4>
                    <div class="lms_social">
                        <ul>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- Member no-07 -->
    </div>
@endsection
