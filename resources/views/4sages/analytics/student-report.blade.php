@extends('layouts.header')
@section('title', 'View Course Result')

@section('content')
    <script src="/js/quiz.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card-box">

                    <h4 class="m-t-0 header-title"><b>Line Chart</b></h4>
                    <p class="text-muted m-b-15 font-13">A line chart is a way of plotting data points on a line.
                        Often, it is used to show trend data, and the comparison of two data sets.
                    </p>

                    <canvas id="lineChart" height="300" width="760" style="width: 760px; height: 300px;"></canvas>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card-box">

                    <h4 class="m-t-0 header-title"><b>Bar Chart</b></h4>
                    <p class="text-muted m-b-15 font-13">A bar chart is a way of showing data as bars.
                        It is sometimes used to show trend data, and the comparison of multiple data sets side by side.
                    </p>

                    <canvas id="bar" height="300" width="760" style="width: 760px; height: 300px;"></canvas>
                </div>
            </div>
        </div>
    </div>

@endsection
