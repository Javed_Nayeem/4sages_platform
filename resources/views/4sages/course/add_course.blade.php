@extends('layouts.header')
@section('title', 'Create New Course')

@section('content')
  <div class="container">
    <div class="row">

        <?php
        echo call_create_course();
        ?>

    </div>
  </div>
@endsection
