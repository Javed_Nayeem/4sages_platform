@extends('layouts.header')
@section('title', 'Curriculum')

@section('content')

  <div class="container container-height">

    <div class="lms_course_syllabus lms_offer_courses">

      @auth
      @if (Auth::user()->role == "Admin")
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="lms_title_center">
              <div class="lms_heading_1 top-margin-50">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_course_modal">Add Curriculum</button>
              </div>
            </div>
          </div>
        </div>
      @endif
      @endauth

      <div class="row">
        @foreach($courses as $course)
          <div class="col-lg-4 col-md-4 col-sm-6 top-margin-50">
            <a href="/course/{{ $course->course_id }}">
              <div class="lms_video">
                <div class="lms_hover_section">
                  <img class="radius-250" src="/course/thumbnail/{{ $course->image_title }}">
                </div>
                <div class="col-md-12 text-center"><h1>{{ $course->course_name }}</h1></div>
              </div>
            </a>
          </div>
        @endforeach

      </div>
    </div>
  </div>

@endsection