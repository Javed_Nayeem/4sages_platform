@extends('layouts.header')
{{--<script type="text/javascript" src="/js/plugin/owl-carousel/owl.carousel.js"></script>--}}
@section('title', "$course_data->course_name")

@section('content')
  <script src="/js/course.js"></script>
  <script src="/js/syllabus.js"></script>
  <script src="/js/quiz.js"></script>

  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  <input type="hidden" id="course_id" value="{{ $course_data->course_id }}">

  <div class="container container-height">


    <!-- edit course Modal -->
    <div class="modal fade" id="edit_course_modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 200px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Edit Course</h4>
          </div>
          <div class="modal-body">


            <div class="form-group">
              <label for="course_name">Course Name</label>
              <input type="text" class="form-control" id="course_name" placeholder="Course Name"
                     value="{{$course_data->course_name}}">
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" id="description" placeholder="Description"
                     value="{{$course_data->description}}">
            </div>

            <div class="form-group">
              <label for="course_overview">Course Overview</label>
              <textarea class="form-control" id="course_overview"
                        placeholder="Course Overview"><?php echo $course_data->overview; ?></textarea>
            </div>

            <div class="form-group">
              <label for="overview">Image</label>
              <input type="file" class="form-control" id="image" name="image" placeholder="Course Image" required>
            </div>



          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" id="course_update" class="btn btn-default"
                    data-dismiss="modal">Save</button>
          </div>
        </div>

      </div>
    </div>
    <!-- end Modal -->

    <!-- Add student Modal -->
    <div class="modal fade" id="add_student_modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 200px;">
          <div class="modal-header">

            <h4 class="modal-title">Add Student</h4>
          </div>
          <div class="modal-body">
            <table class="table" id="add_student_table">

              <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Email</th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($users as $user) {


                  $flag = DB::select( DB::raw("select * from course_student where student_id=$user->id AND
                  course_id=$course_data->course_id") );

                  echo '<tr>
                        <td><input type="checkbox" id="chk" value="'.$user->id.'"';
                  if ($flag != NULL) echo " checked";
                  echo '></td>
                        <td>'.$user->name.'</td>
                        <td>'.$user->email.'</td>
                    </tr>';
              }
              ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" id="add_student_button" class="btn btn-default"
                    data-dismiss="modal">Add</button>
          </div>
        </div>

      </div>
    </div>
    <!-- end Modal -->

    <!-- add syllabus Modal -->
    <div class="modal fade" id="add_syllabus_modal" role="dialog">
      <div class="modal-dialog">

        <form action="/add/syllabus" method="post" enctype="multipart/form-data">
          <div class="modal-content" style="margin-top: 200px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add syllabus</h4>
            </div>
            <div class="modal-body">
              <div class="lms_login_body">

                {{ csrf_field() }}

                <input type="hidden" name="course_id" id="course_id" value="{{ $course_data->course_id }}">

                <div class="form-group">
                  <label for="unit_title">Unit Title</label>
                  <input type="text" class="form-control" id="unit_title" name="unit_title"
                         placeholder="Unit Title">
                </div>

                <div class="form-group">
                  <label for="unit_overview">Unit Overview</label>
                  <input type="text" class="form-control" id="unit_overview" name="unit_overview" placeholder="Unit Overview">
                </div>

                <div class="form-group">
                  <label for="unit_image">Image</label>
                  <input type="file" class="form-control" id="unit_image" name="unit_image">
                </div>
              </div>
              <!-- all the content here-->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <input type="submit" class="btn btn-default" value="Save">
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- end Modal -->

    <!-- add quiz Modal -->
    <div class="modal fade" id="add_quiz_modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 200px; width: 190%; left: -40%;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Quiz</h4>
          </div>
          <div class="modal-body">
            <!-- all the content here-->

            <div class="form-group">
              <label for="quiz_title">Quiz Title</label>
              <input type="text" class="form-control" id="quiz_title" placeholder="Quiz Title" required>
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                <div id="education_fields">
                </div>
                <div class="col-sm-3 nopadding">
                  <div class="form-group">
                    <label>Question</label>
                    <input type="text" class="form-control" name="question[]" value="" placeholder="Question">
                  </div>
                </div>
                <div class="col-sm-3 nopadding">
                  <div class="form-group">
                    <label>Option 1</label>
                    <input type="text" class="form-control" name="option[]" value="" placeholder="Option 1">
                  </div>
                </div>
                <div class="col-sm-3 nopadding">
                  <div class="form-group">
                    <label >Option 2</label>
                    <input type="text" class="form-control" name="option[]" value="" placeholder="Option 2">
                  </div>
                </div>

                <div class="col-sm-3 nopadding">
                  <div class="form-group">
                    <div class="input-group">
                      <label>Answer</label>
                      <input type="text" class="form-control" name="answer[]" value="" placeholder="Answer">
                      <div class="input-group-btn ">
                        <button class="btn btn-success add_question_button" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                      </div>
                    </div>
                  </div>
                </div>
                <!--    <div class="col-sm-3 nopadding">
                      <div class="form-group">
                        <div class="input-group">
                          <label>Question Type</label>
                          <select class="form-control" id="type" name="type[]">
                            <option value="mcq">Multiple Choice Questions</option>
                            <option value="text">Text</option>
                          </select>
                          <div class="input-group-btn ">
                            <button class="btn btn-success add_question_button" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                          </div>
                        </div>
                      </div>
                    </div> -->

                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-default" id="question_save" data-dismiss="modal">Save</button>
          </div>
        </div>

      </div>
    </div>
    <!-- end Modal -->

    <!-- invitation course Modal -->
    <div class="modal fade" id="invite_course_modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 200px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Send Invitation</h4>
          </div>
          <div class="modal-body">
            <!-- all the content here-->

            <div class="form-group">
              <label>Email</label>
              <input type="text" class="form-control" name="email" id="invite_email" value="" placeholder="email">

            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" id="course_invite" class="btn btn-default" data-dismiss="modal">Send
              Invitation</button>
          </div>
        </div>

      </div>
    </div>
    <!-- end Modal -->


    <div class="row">
      <div class="col-lg-12 text-center top-margin-50">
        @auth
        @if (Auth::user()->role == "Admin")
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_student_modal">Add Student</button>
          {{--<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_quiz_modal">Add Quiz</button>--}}
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit_course_modal">Edit Course</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#invite_course_modal">Invite</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_syllabus_modal">Add Syllabus</button>
          <button class="btn btn-default" id="course_delete">Delete Course</button>
        @endif
        @endauth
        <h4 id="message_display"></h4>
      </div>
    </div>






    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 lms_title_center lms_heading_1">
        <h1 style="font-size: 40px">Curriculum Syllabus</h1>
      </div>
    </div>

    @if(count($syllabus_data) == 0)
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 lms_title_center lms_heading_1">
          <h3>Please Add New Syllabus</h3>
        </div>
      </div>
  @endif


  <!-- Syllabus List -->
    <div class="row">
      <div class="col-lg-12 top-margin-50">
        <div class="lms_course_syllabus">
          <div class="row" id="syllabus_list">

            @foreach ($syllabus_data as $syllabus)
              <div class="col-lg-4 col-md-4 col-sm-6" id="syllabus_{{ $syllabus->id }}">
                <a href="/course/tutorial/{{$syllabus->id }}">
                  <div class="lms_video">
                    <div class="lms_hover_section"> <img src="/syllabus/preview/{{$syllabus->unit_image }}" style="border-radius: 10px;">
                    </div>
                    <h2 style="font-family: 'Open Sans'"> {{ $syllabus->unit_title }}</h2>
                    <h4 style="font-family: Tahoma; letter-spacing: 1px;" id="week_{{  $syllabus->id }}">
                      @if (strlen($syllabus->unit_overview) > 100)
                        {{ substr($syllabus->unit_overview, 0, 100) . " ..."}}
                      @else {{ $syllabus->unit_overview }}
                      @endif
                    </h4>
                  </div>
                </a>
              </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>


  </div>


@endsection