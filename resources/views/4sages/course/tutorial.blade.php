@extends('layouts.header')
{{--<script type="text/javascript" src="/js/plugin/owl-carousel/owl.carousel.js"></script>--}}
@section('title', "$syllabus_data->unit_title")

@section('content')
  <script src="/js/course.js"></script>
  <script src="/js/syllabus.js"></script>
  <script src="/js/tutorial.js"></script>

  <link href="/css/magic-check.css" rel="stylesheet">

  <input type="hidden" id="c_time" value="{{ time() }}">

  <!-- add syllabus content Modal -->
  <div class="modal fade" id="add_content_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="margin-top: 200px; width: 160%">
        <form action="/syllabus/add" method="post" enctype="multipart/form-data">

          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          <input type="hidden" name="syllabus_id" id="syllabus_id" value="{{ $syllabus_data->id }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Content</h4>
          </div>
          <div class="modal-body">
            <!-- all the content here-->

            <div class="form-group">
              <label for="content_title">Content Title</label>
              <input type="text" class="form-control" name="content_title" placeholder="Content Title" id="content_title" required>
            </div>

            <div class="form-group">
              <label for="content_area">Content Area</label>
              <textarea class="form-control" id="content_area" name="content_area" style="height: 100px"
                        placeholder="Content
            Area">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</textarea>
            </div>

            <table class="table">

              <tbody>

              <tr>
                <td><input type="text" class="form-control" placeholder="Start time" name="start_1"></td>
                <td><input type="text" class="form-control" placeholder="Question" name="question_1"></td>
                <td><input type="text" class="form-control" placeholder="Option 1" name="q1_option_1" id="q1_option_1"></td>
                <td><input type="text" class="form-control" placeholder="Option 2" name="q1_option_2" id="q1_option_2"></td>
                <td><input type="text" class="form-control" placeholder="Option 3" name="q1_option_3" id="q1_option_3"></td>

                <td>
                  <select id="option_1_select_ans" name="option_1_select_ans" class="form-control" style="width: 150px">
                    <option id="q1_ans_1"></option>
                    <option id="q1_ans_2"></option>
                    <option id="q1_ans_3"></option>
                  </select>
                </td>
              </tr>

              <tr>
                <td><input type="text" class="form-control" placeholder="Start time" name="start_2"></td>
                <td><input type="text" class="form-control" placeholder="Question" name="question_2"></td>
                <td><input type="text" class="form-control" placeholder="Option 1" name="q2_option_1" id="q2_option_1"></td>
                <td><input type="text" class="form-control" placeholder="Option 2" name="q2_option_2" id="q2_option_2"></td>
                <td><input type="text" class="form-control" placeholder="Option 3" name="q2_option_3" id="q2_option_3"></td>

                <td>
                  <select id="option_2_select_ans" name="option_2_select_ans" class="form-control" style="width: 150px">
                    <option id="q2_ans_1"></option>
                    <option id="q2_ans_2"></option>
                    <option id="q2_ans_3"></option>
                  </select>
                </td>
              </tr>

              <tr>
                <td><input type="text" class="form-control" placeholder="Start time" name="start_3"></td>
                <td><input type="text" class="form-control" placeholder="Question" name="question_3"></td>
                <td><input type="text" class="form-control" placeholder="Option 1" name="q3_option_1" id="q3_option_1"></td>
                <td><input type="text" class="form-control" placeholder="Option 2" name="q3_option_2" id="q3_option_2"></td>
                <td><input type="text" class="form-control" placeholder="Option 3" name="q3_option_3" id="q3_option_3"></td>

                <td>
                  <select id="option_3_select_ans" name="option_3_select_ans" class="form-control" style="width: 150px">
                    <option id="q3_ans_1"></option>
                    <option id="q3_ans_2"></option>
                    <option id="q3_ans_3"></option>
                  </select>
                </td>
              </tr>



              </tbody>
            </table>


            <div class="form-group">
              <label for="content_media">Media</label>
              {{--<input type="text" class="form-control" placeholder="Youtube Link" id="youtube_link" name="youtube_link"> Or--}}
              <input type="file" class="form-control" id="content_media" name="content_media" >
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <input type="submit" id="add_content_button" class="btn btn-default" value="Add">
          </div>


        </form>

      </div>

    </div>
  </div>
  <!-- end Modal -->


  <!-- Flash Card Modal -->
  <div class="modal fade" id="flash_card_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="margin-top: 150px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">x</button>
          <h4 class="modal-title" id="fc_title">Flash Card</h4>
        </div>
        <div class="modal-body">

          <div class="form-group">
            {{--<h3>Description</h3>--}}
            <h4 id="fc_description">this is flashcard description</h4>
          </div>

          <div class="form-group">
            <h3></h3>
            <img id="fc_image" src="" width="100%">
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Add Flash Card Modal -->
  <div class="modal fade" id="add_flashcard_modal" role="dialog">
    <div class="modal-dialog">

      <form method="post" action="/add/flashcard" enctype="multipart/form-data">
        <div class="modal-content" style="margin-top: 200px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h4 class="modal-title">Add Flash Card</h4>
            <input type="hidden" id="flashcard_syllabus_id" name="flashcard_syllabus_id" value="{{ $syllabus_data->id }}">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          </div>
          <div class="modal-body">


            <div class="form-group">
              <label for="course_name">FlashCard Title</label>
              <input type="text" class="form-control" name="flashcard_title" placeholder="FlashCard Title">
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" name="flashcard_description" placeholder="FlashCard Description">
            </div>

            <div class="form-group">
              <label for="description">Image</label>
              <input type="file" class="form-control" id="image" name="image"  required>
            </div>



          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{--<button type="submit" class="btn btn-default">Add</button>--}}
            <input type="submit" class="btn btn-default" value="Add">

          </div>
        </div>
      </form>
    </div>
  </div>


  <!-- Question Modal -->
  <div class="modal fade" id="question_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" id="question_modal_top" style="opacity: 0.5 !important;background-color: black; ">
        <div class="modal-header" style="border-bottom: none">
          <button type="button" class="close" data-dismiss="modal">x</button>
          <h3 class="modal-title" style="color: white;"></h3>
        </div>
        <div class="modal-body" style="padding-top: 1%;padding-left: 54px;">



          <div id="question_div" >
            <h1 id="message_output" style="color: white;">This is the question?</h1>
            <input type="radio" id="question_div_o1" name="student_answer"> <span id="option1" style="color: white; font-size: 230%;">This is Option 1</span> <br>
            <input type="radio" id="question_div_o2" name="student_answer"> <span id="option2" style="color: white; font-size: 230%;">This is option 2</span> <br>
            <input type="radio" id="question_div_o3" name="student_answer"> <span id="option3" style="color: white; font-size: 230%;">This is option 3</span> <br>

          </div>

          <div id="question_div_message">
            <h1 id="message_output_feedback" style="color: white"></h1>
            <h2 id="message_output_feedback_2" style="color: white"></h2>
          </div>


        </div>
        <div class="modal-footer" style="border-top: none; margin-top: -4%">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="question_modal_close_button" style="color: white; font-size: 200%;">Close</button>
          <button type="button" class="btn btn-default" id="tutorial_ans_submit" style="color: white; font-size: 200%;">Submit</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Add Files Modal -->
  <div class="modal fade" id="add_files_modal" role="dialog">
    <div class="modal-dialog">

      <form method="post" action="/add/content/files" enctype="multipart/form-data">
        <div class="modal-content" style="margin-top: 200px;">
          <div class="modal-header">
            {{--<button type="button" class="close" data-dismiss="modal">x</button>--}}
            <h4 class="modal-title">Add Files</h4>
            <input type="hidden" name="syllabus_id" value="{{ $syllabus_data->id }}">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          </div>
          <div class="modal-body">


            <div class="form-group">
              <label for="course_name">Title</label>
              <input type="text" class="form-control" name="file_title" placeholder="File Title">
            </div>


            <div class="form-group">
              <label for="description">File</label>
              <input type="file" class="form-control" id="image" name="image"  required>
            </div>



          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{--<button type="submit" class="btn btn-default">Add</button>--}}
            <input type="submit" class="btn btn-default" value="Upload">

          </div>
        </div>
      </form>
    </div>
  </div>


  <div class="container container-height">



    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="lms_title_center">
          <div class="lms_heading_1" style="margin-top: 50px">
            @if (Auth::user()->role == "Admin")
              {{--<h2 class="lms_heading_title" id="index_course_name">{{ $syllabus_data->unit_title }}</h2>--}}
              @if(count($content_data) == 0)
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_content_modal">Add Content</button>
              @endif

              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit_course_modal">Edit Syllabus</button>

              @if(count($content_data) != 0)
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_flashcard_modal">Add Flash Card</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_files_modal">Add Files</button>
                {{--<button type="button" class="btn btn-default" data-toggle="modal" data-target="#question_modal">Question Modal</button>--}}
                <a class="btn btn-default" href="/add/quiz/{{ $syllabus_data->id }}">Add Quiz</a>
              @endif
              <button class="btn btn-default" id="syllabus_delete">Delete Syllabus</button>
            @endif

          </div>
          <p style="font-size: 25px" id="index_course_description">{{ $syllabus_data->unit_overview }}</p>
          <p id="message_display"></p>
        </div>
      </div>
    </div>

    @if(count($content_data) == 0)
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
          <h3>Please Add Content</h3>
        </div>
      </div>
    @endif


    @foreach($content_data as $content)

      <div class="row top-buffer">



        @if($content->media != Null)

          <div class="col-md-10 col-md-offset-1 top-buffer">
            <div class="lms_video_player">

              @if(is_youtube_link($content->media))

                <object width="100%" height="60%" data="https://www.youtube.com/embed/{{get_youtube_link
                ($content->media)}}">
                </object>

              @else



                <table class="table">

                  <tbody>
                  <tr>
                    <td style="width: 80%">
                      <video id="video_content" controls width="100%" >
                        <source type="video/mp4" src="/content/{{$content->media}}"/>
                      </video>

                    </td>

                  </tr>

                  </tbody>
                </table>

                <div class="lms_element_animation">
                  <div class="col-lg-12">
                    {{--<h3>{{$content->content_title}}</h3>--}}
                    <hr>
                  </div>
                  {{--<div class="col-lg-3 col-md-4 col-sm-4"> <img class="tutorial_logo" src="/images/slider/revolution/laptop.png"/> </div>--}}
                  <div class="col-lg-12 col-md-8 col-sm-8">
                    <p id="sc_description">{{$content->content_description}}</p>
                  </div>
                </div>





                @foreach($files_data as $file)
                  <div class="row" >
                    <div class="col-lg-12" style="padding-top: 50px;">
                      <h2>{{$file->file_title}}</h2>

                      @if ($file->post_mime_type == "application/pdf")
                        <embed src="/content/files/{{$file->file_name}}" width="100%" height="1150" type='application/pdf'>

                          @elseif(strpos($file->post_mime_type, 'image') === 0)
                            <img src="/content/files/{{$file->file_name}}" width="100%">

                          @elseif(strpos($file->post_mime_type, 'video') === 0)
                            <video id="video_file" controls width="100%" >
                              <source type="video/mp4" src="/content/files/{{$file->file_name}}"/>
                            </video>

                          @elseif(strpos($file->post_mime_type, 'text') === 0 )
                            <a href="/content/files/{{$file->file_name}}">Download File</a>

                      @endif
                    </div>
                  </div>
                @endforeach




                {{--Flash Card Buttons--}}
                {{--<div class="row" >--}}
                {{--<div class="col-lg-8 col-lg-offset-2 text-center" style="padding-top: 50px;">--}}
                {{--@foreach($flashcard_data as $flashcard)--}}
                {{--<button type="button" class="btn btn-default" onclick='flashcard_view("{{$flashcard->flashcard_title}}", "{{$flashcard->flashcard_description}}", "{{$flashcard->media}}")'>{{$flashcard->flashcard_title}}</button>--}}
                {{--@endforeach--}}
                {{--</div>--}}
                {{--</div>--}}


                    <?php

                    for ($i=0; $i<count($flashcard_data);  ) {
                        echo '<div class="row" >
                  <div class="col-lg-12 text-center div-to-center" style="padding-top: 50px;display: flex;">';
                        if (isset($flashcard_data[$i])) {
                            echo '<div class="ObjectContainer" ';
                            echo "onclick=\"flashcard_view('".$flashcard_data[$i]->flashcard_title."', ";
                            echo "'".$flashcard_data[$i]->flashcard_description."',";
                            echo "'".$flashcard_data[$i]->media."')\">";
                            echo '<div class="Object div_1">'.$flashcard_data[$i]->flashcard_title.'</div>
                    </div>';
                            $i++;
                        }

                        if (isset($flashcard_data[$i])) {
                            echo '<div class="ObjectContainer" ';
                            echo "onclick=\"flashcard_view('".$flashcard_data[$i]->flashcard_title."', ";
                            echo "'".$flashcard_data[$i]->flashcard_description."',";
                            echo "'".$flashcard_data[$i]->media."')\">";
                            echo '<div class="Object div_2">'.$flashcard_data[$i]->flashcard_title.'</div>
                    </div>';
                            $i++;
                        }

                        if (isset($flashcard_data[$i])) {
                            echo '<div class="ObjectContainer" ';
                            echo "onclick=\"flashcard_view('".$flashcard_data[$i]->flashcard_title."', ";
                            echo "'".$flashcard_data[$i]->flashcard_description."',";
                            echo "'".$flashcard_data[$i]->media."')\">";
                            echo '<div class="Object div_3">'.$flashcard_data[$i]->flashcard_title.'</div>
                    </div>';
                            $i++;
                        }
                        echo '</div></div>';
                    }

                    ?>


                {{--<div class="row" >--}}
                {{--<div class="col-lg-12 col-lg-offset-1 text-center" style="padding-top: 50px;display: flex;">--}}
                {{--<div class="ObjectContainer">--}}
                {{--<div class="Object div_1"><span id="testing_purpose" class="animated flash">Nucleus</span></div>--}}
                {{--</div>--}}

                {{--<div class="ObjectContainer">--}}
                {{--<div class="Object div_2"><span>Electron</span></div>--}}
                {{--</div>--}}

                {{--<div class="ObjectContainer">--}}
                {{--<div class="Object div_3">Proton</div>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}



                {{-- this is animation div
                  <div class="row" >
                    <div class="col-lg-12" style="padding-top: 50px; height: 306px;">

                      <div id="test_div" style="width:300px;position: absolute">
                        <img src="/animation_char/anim_char_3.gif" width="100%">
                      </div>

                    </div>
                  </div>
                --}}

                @if(count($quiz_data) != 0)
                  <div class="row" >
                    <div class="col-lg-12 top-margin-50 text-center">
                      <h1></h1>
                    </div>
                  </div>



                  <div class="row" >
                    <div class="col-lg-8 col-lg-offset-2 text-center top-margin-50">

                      @php $quiz_count = 1; @endphp

                      @foreach($quiz_data as $quiz)
                        <button style="margin: 10px; width: 40%; height: 95px; font-size: 50px;" onclick="quiz({{$quiz->quiz_view_id}})" class="btn btn-lg btn-info">Quiz {{  $quiz_count++ }}</button>
                      @endforeach


                      <div class="col-lg-12 top-margin-50 text-center">
                      <a href="/course/result/{{ $syllabus_data->id }}" style="width: 40%; height: 70px; font-size: 30px;" class="btn btn-lg btn-info">Quiz Reports</a>
                      </div>

                    </div>
                  </div>

                  {{--<form id="quiz_form" action="/course/quiz/" method="post">--}}
                  {{--{{ csrf_field() }}--}}
                  {{--<input type="hidden" id="course_quiz" name="course_quiz">--}}
                  {{--</form>--}}

                  {!! Form::open(['url' => '/course/quiz/', 'id' => 'quiz_form']) !!}
                  <input id="course_quiz" name="course_quiz" type="hidden" >
                  {!! Form::close() !!}
                @endif

              @endif

            </div>

          </div>

        @endif

      </div>

    @endforeach

  </div>

@endsection