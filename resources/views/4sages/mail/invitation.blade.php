<a href=""><h2>Invitation To 4sages</h2></a>

<p>Welcome to 4sages</p>
<p>We would like to invite you to take part in {{$invitation['invitation_course_name']}}.</p>

<p>As a planning step, we’re trying to identify and understand the pressures and opportunities faced by kids in our
    area. So we’re asking a few people to come to the course.</p>


<p>Thank You</p>
<p>{{$invitation['invitation_from_name']}}</p>
