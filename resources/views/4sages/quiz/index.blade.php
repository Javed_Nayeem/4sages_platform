@extends('layouts.header')
@section('title', "Quiz")

@section('content')
  <script src="/js/quiz.js"></script>


  <!-- add blog modal -->

  <div class="modal fade" id="add_blog_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="margin-top: 150px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">x</button>
          <h4 class="modal-title" id="fc_title">Add New Blog</h4>
        </div>
        <div class="modal-body">

          <div class="form-group">
            <label for="new_blog_name">Blog Name</label>
            <input type="text" class="form-control" id="new_blog_name" placeholder="Blog Name">
          </div>

          <div class="form-group">
            <label for="new_blog_link">Blog Link</label>
            <input type="text" class="form-control" id="new_blog_link" placeholder="Blog Link">
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" id="add_new_blog">Add</button>
        </div>

      </div>
    </div>
  </div>


  <div class="container">

    {!! Form::open(['url' => '/add/question/', 'files' => true]) !!}


    <input type="hidden" name="course_topic_id" value="{{$course_id}}">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <div class="row" id="question_table">

      <div class="col-lg-12 text-center top-margin-50" id="question_1">
        <div class="row">
          <div class="form-group">
            <label class="col-md-2 control-label">Question 1</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <textarea class="form-control" name="question[]" placeholder="Write your question here" rows="3" cols="200" required></textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Option 1</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" name="option[]" placeholder="Option 1" required>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Option 2</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" name="option[]" placeholder="Option 2" required>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Option 3</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" name="option[]" placeholder="Option 3" required>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Correct Answer</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" name="answer[]" placeholder="Correct Answer" required>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Media</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
                <input type="file" class="form-control" name="media_1" value="default" accept="image/*">
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Explanation</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <textarea class="form-control" name="explanation[]" placeholder="Explanation for this question answer" rows="3" cols="200" required></textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-2 control-label">Explanation Media</label>
            <div class="col-md-3 inputGroupContainer">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
                <input type="file" class="form-control" name="explanation_media_1" value="default" accept="image/*, video/*">
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-12 text-center top-margin-50">
      <div class="row" >
        <h2>Related Blog Link</h2>
        <h4 id="no_blog_text">No Blog Link Attached</h4>
        <ul class="lms_underlist_doted" id="blog_list" style="display: none">

        </ul>
      </div>
    </div>


    <div class="col-lg-12 text-center top-margin-50">
      <div class="row" >
        <div class="form-group">
          <button type="button" class="btn btn-primary" id="add_new_question"><i class="fa fa-plus"></i> &nbsp Add New Question</button>
          <button type="button" class="btn btn-primary" id="add_new_blog_link" data-toggle="modal" data-target="#add_blog_modal"><i class="fa fa-plus"></i> &nbsp Add Blog</button>
          <button type="button" class="btn btn-danger" id="remove_question"><i class="fa fa-minus"></i> &nbsp Remove Question</button>
        </div>
      </div>

      <div class="row top-margin-10">
        <div class="form-group">
          <input type="submit" class="btn btn-lg btn-success" value="Save" style="width: 200px">
        </div>
      </div>
    </div>

    {!! Form::close() !!}
  </div>


@endsection