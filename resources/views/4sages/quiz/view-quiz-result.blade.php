@extends('layouts.header')
@section('title', 'View Quiz Result')

@section('content')
    <script src="/js/quiz.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 top-margin-80">
                <div class="lms_default_tab">

                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane text-center active" id="myAnalytics">

                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <h1> {{ $data->unit_title }}</h1>
                                <h3 class="top-margin-30">You are ahead of {{ $data->percentile }}% scholars who took this quiz</h3>
                                <hr>
                                <canvas id="myChart" style="display: block; width: 759px; height: 379px;" height="379" width="759"></canvas>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">

                                @if($next_quiz_id != 0)
                                    <h3>Do you want to take another quiz?</h3>
                                    <div class="col-lg-12">
                                        <div class="lms_title_center">
                                            <div class="lms_heading_1" style="margin-top: 30px">
                                                <button type="button" class="btn btn-lg btn-success" id="another_quiz">Yes</button>
                                                <a type="button" class="btn btn-lg btn-success" href="/course/tutorial/{{ $data->syllabus_id }}">No</a>
                                            </div>
                                        </div>
                                    </div>

                                    {!! Form::open(['url' => '/course/quiz/', 'id' => 'quiz_form']) !!}
                                    <input id="course_quiz" name="course_quiz" type="hidden" value="{{ $next_quiz_id }}">
                                    {!! Form::close() !!}

                                @else
                                    <div class="col-lg-12">
                                        <div class="lms_title_center">
                                            <div class="lms_heading_1" style="margin-top: 30px">
                                                <a type="button" class="btn btn-lg btn-success" href="/course/tutorial/{{ $data->syllabus_id }}">Back To Topic</a>
                                            </div>
                                        </div>
                                    </div>

                                @endif

                            </div>



                            <script>

                                ctx = document.getElementById("myChart").getContext('2d');

                                var myChart = new Chart(ctx, {
                                    type: 'horizontalBar',
                                    data: {
                                        labels: ["Scored {{ $data->score }}%" ],
                                        datasets: [{
                                            label: '%',
                                            data: [{{ $data->score }}],
                                            backgroundColor: [
                                                'rgba(255, 99, 132, 0.2)'
                                            ],
                                            borderColor: [
                                                'rgba(255,99,132,1)',                                        ],
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        legend: { display: false },

                                        scales: {
                                            xAxes: [{
                                                ticks: {
                                                    fontSize: 30,
                                                    beginAtZero:true,
                                                    min: 0,
                                                    max: 100,
                                                    callback: function(value) {
                                                        return value + "%"
                                                    }
                                                }
                                            }],
                                            yAxes: [{
                                                barPercentage: 0.30,
                                                ticks: {
                                                    fontSize: 30,
                                                    //mirror: true
                                                },
                                            }]
                                        }
                                    }
                                });
                            </script>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
