@extends('layouts.header')
@section('title', '')

@section('content')
    <script src="/js/register.js"></script>

    <div class="container container-height">
        <div class="row">
            <div class="col-lg-12">
                {{--<h2>Register with <span class="lms_label">4Sages</span> and share your <span class="lms_label">Experience</span>...!</h2>--}}
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="lms_login_window lms_login_light">
                    <h3 style="font-size: 25px">Sign In</h3>
                    <div class="lms_login_body">
                        <form role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="login_email">Email address</label>
                                <input id="email" placeholder="Enter email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="login_pass">Password</label>
                                <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Login</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="lms_register_window lms_register_light">
                    <h3 style="font-size: 25px">create account</h3>
                    <div class="lms_register_body">

                        <form role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">Name</label>
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="email">E-mail address</label>
                                        <input id="email" type="email" class="form-control" name="email" required>
                                        {{--@if ($errors->has('email'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                        {{--</span>--}}
                                        {{--@endif--}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-lg-6 col-md-6">
                                    <label for="signup_pass">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-lg-6 col-md-6">
                                    <label for="signup_repass">Re-enter Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="email">Account Type</label>
                                        <select id="account_type" name="account_type" class="form-control">
                                            <option value="1">Free Account</option>
                                            <option value="0">Subscription</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row" id="payment_info_div" style="display: none;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name">Payment Information</label>
                                        <input id="payment_info" type="text" class="form-control" name="payment_info">
                                    </div>
                                </div>
                            </div>



                            <button type="submit" class="btn btn-default">Create Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
