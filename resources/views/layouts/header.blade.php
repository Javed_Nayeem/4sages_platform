<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta name="description"  content="Learning System"/>
  <meta name="keywords" content="">
  <meta name="MobileOptimized" content="320">

  <!-- favicon links -->
  <link rel="shortcut icon" href="/images/4sages-icon.png">

  <!-- main css -->
  <link rel="stylesheet" href="/css/main.css" media="screen"/>
  <link rel="stylesheet" href="/css/core-style.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css"/>

  <!-- Sweet Alert css -->
{{--<link href="/js/plugin/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />--}}

<!-- Sweet Alert css -->
  <link href="/css/sweetalert2.css" rel="stylesheet" type="text/css" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
{{--<script type="text/javascript" src="/js/plugin/owl-carousel/owl.carousel.js"></script>--}}

<!--plugin-->
  <script type="text/javascript" src="/js/plugin/appear/jquery.appear.js"></script>
  <script type="text/javascript" src="/js/plugin/count/jquery.countTo.js"></script>
  {{--<script src="http://maps.googleapis.com/maps/api/js"></script>--}}
  <script type="text/javascript" src="/js/plugin/mediaelement/mediaelement-and-player.js"></script>
  <script type="text/javascript" src="/js/plugin/mixitup/jquery.mixitup.js"></script>
  <script type="text/javascript" src="/js/plugin/modernizr/modernizr.custom.js"></script>
  <!-- <script type="text/javascript" src="js/plugin/owl-carousel/owl.carousel.js"></script> -->
  <script type="text/javascript" src="/js/plugin/parallax/jquery.stellar.js"></script>
  <script type="text/javascript" src="/js/plugin/prettyphoto/js/jquery.prettyPhoto.js"></script>
  <script type="text/javascript" src="/js/plugin/revslider/js/jquery.themepunch.plugins.min.js"></script>
  <script type="text/javascript" src="/js/plugin/revslider/js/jquery.themepunch.revolution.js"></script>
  <script type="text/javascript" src="/js/plugin/single/single.js"></script>

  <script type="text/javascript" src="/js/plugin/wow/wow.js"></script>
  <script type="text/javascript" src="/js/grid-gallery.js"></script>
  <script type="text/javascript" src="/js/bootstrap.js"></script>
{{--<script type="text/javascript" src="/js/custom.js"></script>--}}
<!--main js file end-->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


  <!-- Scripts -->
  <script src="/js/app.js"></script>


  <title>4sages</title>
</head>
<!-- Header End -->

<!-- Body Start -->
<body>
<!-- page title Start -->
<div class="lms_page_title">
  <div class="lms_page_title_bg" data-stellar-background-ratio="0.5"></div>
  <div class="lms_page_title_bg_overlay">
    <div class="container">
      <div class="col-md-12 text-center header_title">@yield('title')</div>
    </div>
  </div>
</div>
<!-- page title end -->
<!--Header start-->
<header id="lms_header">
  <div class="container">
    <h1 class="logo" style="margin-top: 10px;"> <a href="https://www.4sages.com"> <img alt="4sages" width="184" height="47" data-sticky-width="82" data-sticky-height="40" src="/images/logo.png"> </a> </h1>
    <button class="lms_menu_toggle btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse"><i class="fa fa-bars"></i> </button>
  </div>

  @include('4sages.partials.topmenu')


</header>
<!--Header end-->

<!--main container start-->

@yield('content')


<!--main container end-->

<!-- Sweet Alert js -->
<script src="/js/plugin/bootstrap-sweetalert/sweetalert2.all.min.js"></script>

@include('4sages.partials.footer')

<a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>

<!--main js file start-->



</body>
<!-- Body End -->
</html>
