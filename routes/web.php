<?php


Route::get('/', function () {
    check_temp_user();
    //return redirect('/login');
    return redirect('/courselist');
    //return view('welcome');
});


Route::get('/team', function () {
    return view('4sages.aboutus.team');
});
Route::get('/aboutus', function () {
    return view('4sages.aboutus.aboutus');
});


Route::get('/login', function () {
    return view('4sages.login.signin');
});



Route::get('/courselist', 'CurriculumController@all_course');
Route::get('/course/{id}', 'CurriculumController@index');


Auth::routes();

//disable default user register
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('authenticated');
//Route::post('register', 'Auth\RegisterController@register')->middleware('authenticated');
//Route::get('/home', 'HomeController@index');

Route::get('/home', function () {
    return redirect("/courselist");
});





/*
 * for testing
 */

Route::get('/test', 'QuizController@calculateScorePercentile');
Route::get('/mail', function () {
    return view('4sages.mail.invitation');
});
Route::get('/sendmail', 'InvitationController@testmail');





/*
 * profile and user routes
 */

Route::get('/profile', 'UserController@index');
Route::post('/edituser', 'UserController@edit');
Route::post('/edit/user/status', 'UserController@editUserStatus');
Route::post('/update/user/info', 'UserController@edit');





/*
 * course routes
 */

Route::post('/addcourse', 'CourseController@add');
Route::get('/addcourse', function () {
    return view('4sages.course.add_course');
});


Route::post('/editcourse', 'CourseController@edit');
Route::post('/course/sendmessage', 'CourseController@send_message');
Route::post('/course/delete/', 'CourseController@delete');
Route::post('/course/addstudent', 'CourseController@add_student');
Route::post('/editsyllabus', 'CourseController@editsyllabus');
Route::post('/add/syllabus','CourseController@add_syllabus');
Route::post('/syllabus/delete/','CourseController@delete_syllabus');
Route::post('/quiz/delete/{id}','CourseController@delete_quiz');
Route::post('/tutorial/complete','CourseController@tutorialComplete');
Route::post('/upload/profile_photo', 'UserController@upload_profile_picture');
Route::get('/course/tutorial/{id}', 'CourseController@tutorial');
Route::post('/syllabus/add','CourseController@add_syllabus_content');
Route::post('/course/invite', 'InvitationController@index');
Route::post('/get/message/{id}', 'CourseController@getMessage');
Route::post('/add/flashcard', 'CourseController@add_flashcard');
Route::post('/add/content/files', 'CourseController@addContentFile');





/*
 * Quiz routes
 */

Route::get('/add/quiz/{id}', 'QuizController@index');
Route::post('/add/question/', 'QuizController@add_quiz');
Route::post('/course/quiz/', 'QuizController@course_quiz');
Route::post('/store/result', 'QuizController@storeQuizResult');
Route::post('/view/result/', 'QuizController@viewQuizResult');
Route::get('/course/result/{id}', 'QuizController@viewCourseResult');





/*
 * Analytics routes
 */

Route::get('/view/report/{id}', 'ReportController@viewReport');





/*
 * Calendar Routes
 */

Route::get('/calendar', 'EventController@index');





/*
 * top student ranking
 */
Route::get('/topstudent', 'QuizController@ranking');



Route::get('/report/download','UserController@downloadPDF');


